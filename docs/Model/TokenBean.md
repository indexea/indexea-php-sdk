# # TokenBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**name** | **string** |  | [optional]
**token** | **string** |  | [optional]
**scopes** | **object** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**expired_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
