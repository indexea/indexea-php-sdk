# # IntelligentMapping

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**word** | **string** | 关键词 | [optional]
**field** | **string** | 匹配字段 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
