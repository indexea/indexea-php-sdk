# # QueryActionBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**app** | **int** |  | [optional]
**index** | **int** |  | [optional]
**query** | **int** |  | [optional]
**widget** | **int** |  | [optional]
**recomm** | **int** |  | [optional]
**error** | **object** |  | [optional]
**user** | **string** |  | [optional]
**q** | **string[]** |  | [optional]
**referer** | **string** |  | [optional]
**params** | **object** |  | [optional]
**hits** | **int** |  | [optional]
**took** | **int** |  | [optional]
**ip** | **string** |  | [optional]
**nation** | **string** |  | [optional]
**province** | **string** |  | [optional]
**city** | **string** |  | [optional]
**browser** | **string** |  | [optional]
**os** | **string** |  | [optional]
**clicks** | **string[]** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
