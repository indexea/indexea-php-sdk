# # MentorForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** | 成员总数 | [optional]
**mentors** | [**\OpenAPI\Client\modals\AccountBean[]**](AccountBean.md) | 成员列表 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
