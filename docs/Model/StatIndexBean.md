# # StatIndexBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | **int** |  | [optional]
**index** | **int** |  | [optional]
**date** | **\DateTime** |  | [optional]
**storage** | **int** |  | [optional]
**searchs** | **int** |  | [optional]
**errors** | **int** |  | [optional]
**records** | **int** |  | [optional]
**avg_time** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
