# # PaymentService

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**days** | **int** | 服务天数 | [optional]
**storage** | **int** | 存储空间 | [optional]
**shards** | **int** | 分片数 | [optional]
**replicas** | **int** | 副本数 | [optional]
**searchs** | **int** | 搜索次数 | [optional]
**price** | **int** | 服务价格 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
