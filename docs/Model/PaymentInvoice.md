# # PaymentInvoice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**account** | **int** |  | [optional]
**app** | **int** |  | [optional]
**orders** | [**\OpenAPI\Client\modals\PaymentOrder[]**](PaymentOrder.md) |  | [optional]
**price** | **int** |  | [optional]
**type** | **int** |  | [optional]
**status** | **int** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**logs** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
