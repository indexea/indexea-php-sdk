# # IndexTemplate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**account** | **int** |  | [optional]
**name** | **string** |  | [optional]
**intro** | **string** |  | [optional]
**mappings** | **object** |  | [optional]
**query** | **object** |  | [optional]
**widgets** | **object[]** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**opened** | **bool** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
