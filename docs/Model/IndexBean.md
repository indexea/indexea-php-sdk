# # IndexBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**account** | **int** |  | [optional]
**app** | **int** |  | [optional]
**name** | **string** |  | [optional]
**system** | **bool** | 是否系统索引（系统索引不允许用户删除） | [optional]
**intro** | **string** |  | [optional]
**alias** | **string[]** |  | [optional]
**type** | **string** |  | [optional]
**filters** | **object[]** | 索引关联的临时过滤器 | [optional]
**aggs** | **object** | 索引关联的临时聚合器 | [optional]
**sorts** | **object[]** | 索引关联的临时排序 | [optional]
**shards** | **int** | 该索引的分片数 | [optional]
**replicas** | **int** | 该索引的副本数 | [optional]
**analyzer** | **string** |  | [optional]
**search_analyzer** | **string** |  | [optional]
**stat** | [**\OpenAPI\Client\modals\IndexStatBean**](IndexStatBean.md) |  | [optional]
**options** | **object** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
