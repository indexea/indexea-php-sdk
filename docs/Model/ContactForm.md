# # ContactForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **string** |  |
**name** | **string** |  |
**tel** | **string** |  |
**email** | **string** |  | [optional]
**memo** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
