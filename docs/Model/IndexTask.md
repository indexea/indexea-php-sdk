# # IndexTask

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**index** | **int** |  | [optional]
**es_task_id** | **string** |  | [optional]
**es_detail** | **object** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**end_at** | **\DateTime** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
