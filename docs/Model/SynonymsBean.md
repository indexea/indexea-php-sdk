# # SynonymsBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**app** | **int** |  | [optional]
**index** | **int** |  | [optional]
**type** | **int** |  | [optional]
**words** | **string[]** |  | [optional]
**synonyms** | **string[]** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
