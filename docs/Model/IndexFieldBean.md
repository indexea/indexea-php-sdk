# # IndexFieldBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | [**\OpenAPI\Client\modals\IndexBean**](.md) |  | [optional]
**fields** | **object** | 字段列表信息 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
