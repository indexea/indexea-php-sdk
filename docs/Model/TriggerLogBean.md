# # TriggerLogBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**app** | **int** |  | [optional]
**log** | **int** |  | [optional]
**url** | **string** |  | [optional]
**body** | **string** |  | [optional]
**resp_code** | **int** |  | [optional]
**resp_body** | **string** |  | [optional]
**resp_time** | **int** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
