# # CrawlerLog

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**index** | **int** |  | [optional]
**home_url** | **string** |  | [optional]
**begin_at** | **\DateTime** |  | [optional]
**end_at** | **\DateTime** |  | [optional]
**used_seconds** | **int** |  | [optional]
**success_count** | **int** |  | [optional]
**failed_count** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
