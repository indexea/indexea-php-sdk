# # IndexStatBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | [optional]
**index** | **string** |  | [optional]
**health** | **string** |  | [optional]
**status** | **string** |  | [optional]
**records** | **int** |  | [optional]
**storage** | **int** |  | [optional]
**shards** | **int** |  | [optional]
**replicas** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
