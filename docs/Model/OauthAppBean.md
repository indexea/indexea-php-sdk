# # OauthAppBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**account** | **int** |  | [optional]
**app** | **int** |  | [optional]
**ident** | **string** | 三方应用标识 client id | [optional]
**name** | **string** |  | [optional]
**intro** | **string** |  | [optional]
**logo** | **string** |  | [optional]
**url** | **string** | 应用地址 | [optional]
**secret** | **string** | 第三方应用密钥, 该值仅在创建和重置密钥时返回 | [optional]
**callback** | **string** | 回调地址 | [optional]
**scopes** | **string** | 授权范围 | [optional]
**created_at** | **\DateTime** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
