# # BlacklistBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | **int** |  | [optional]
**block_type** | **int** |  | [optional]
**badwords** | **string[]** |  | [optional]
**black_ips** | **string[]** |  | [optional]
**white_ips** | **string[]** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**status** | **int** |  | [optional] [default to self::STATUS_3]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
