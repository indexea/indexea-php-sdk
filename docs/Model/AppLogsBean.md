# # AppLogsBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** | 日志总数 | [optional]
**logs** | [**\OpenAPI\Client\modals\AppLogBean[]**](AppLogBean.md) |  | [optional]
**excel_url** | **string** | 用于导出 Excel 的地址 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
