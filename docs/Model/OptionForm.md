# # OptionForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** | 配置项名称 |
**type** | **string** | 配置值类型 |
**value** | **string** | 配置值 |
**vcode** | **string** | 验证码(非必须，只有在配置通知邮箱和手机的时候才需要) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
