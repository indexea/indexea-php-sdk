# # AppBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**account** | **int** | 创建者 | [optional]
**cluster** | **int** |  | [optional]
**name** | **string** |  | [optional]
**ident** | **string** | 应用唯一标识 | [optional]
**intro** | **string** |  | [optional]
**paid** | **bool** | 是否付费应用 | [optional]
**storage** | **int** | 存储空间 | [optional]
**shards** | **int** | 分片数 | [optional]
**replicas** | **int** | 副本数 | [optional]
**searchs** | **int** | 搜索次数 | [optional]
**mentor** | [**\OpenAPI\Client\modals\AppMentorBean**](AppMentorBean.md) |  | [optional]
**settings** | **object** | 应用设置 | [optional]
**indices** | **int** | 应用索引数 | [optional]
**created_at** | **\DateTime** |  | [optional]
**expired_at** | **\DateTime** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
