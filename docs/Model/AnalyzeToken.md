# # AnalyzeToken

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **string** |  | [optional]
**start_offset** | **float** |  | [optional]
**end_offset** | **float** |  | [optional]
**type** | **string** |  | [optional]
**position** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
