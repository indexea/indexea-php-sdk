# # OpenidBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | 类型 | [optional]
**openid** | **string** | 三方账号唯一标识 | [optional]
**name** | **string** | 三方账号昵称 | [optional]
**created_at** | **\DateTime** | 首次登录时间 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
