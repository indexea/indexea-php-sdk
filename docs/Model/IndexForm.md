# # IndexForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**intro** | **string** |  | [optional]
**alias** | **string[]** |  | [optional]
**type** | **string** |  | [optional]
**shards** | **int** | 该索引的分片数 | [optional]
**replicas** | **int** | 该索引的副本数 | [optional]
**analyzer** | **string** |  | [optional]
**search_analyzer** | **string** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
