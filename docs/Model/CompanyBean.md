# # CompanyBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | 公司名称 | [optional]
**url** | **string** | 公司网址 | [optional]
**nation** | **string** | 国家 | [optional]
**province** | **string** | 省/州/地区 | [optional]
**city** | **string** | 城市 | [optional]
**taxpayer** | **string** | 纳税人识别号 | [optional]
**bank** | **string** | 开户行 | [optional]
**account** | **string** | 银行账号 | [optional]
**address** | **string** | 公司注册地址 | [optional]
**tel** | **string** | 公司电话 | [optional]
**license** | **string** | 营业执照图片地址 | [optional]
**certificate** | **string** | 一般纳税人证明扫描件图片地址 | [optional]
**post_addr** | **string** | 快递地址 | [optional]
**post_code** | **string** | 邮编 | [optional]
**post_name** | **string** | 收件人姓名 | [optional]
**post_tel** | **string** | 收件人电话 | [optional]
**created_at** | **\DateTime** | 首次填写时间 | [optional]
**updated_at** | **\DateTime** | 最后更新时间 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
