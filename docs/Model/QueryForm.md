# # QueryForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**intro** | **string** |  | [optional]
**indices** | **int[]** | 关联的索引列表 | [optional]
**query** | **object** | 查询条件 | [optional]
**suggest** | **object** |  | [optional]
**script_score** | **object** |  | [optional]
**script_fields** | **object** |  | [optional]
**sort_fields** | [**\OpenAPI\Client\modals\QuerySortField[]**](QuerySortField.md) |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
