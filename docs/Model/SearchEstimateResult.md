# # SearchEstimateResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_purchase_searchs** | **int** |  | [optional]
**last_purchase_searchs** | **int** |  | [optional]
**avail_searchs** | **int** |  | [optional]
**average_searchs** | **int** |  | [optional]
**total_searchs** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
