# # CrawlerLogs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** |  | [optional]
**logs** | [**\OpenAPI\Client\modals\CrawlerLog[]**](CrawlerLog.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
