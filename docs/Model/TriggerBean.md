# # TriggerBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | **int** |  | [optional]
**url** | **string** |  | [optional]
**triggers** | **string** |  | [optional]
**password** | **string** |  | [optional]
**all_triggers** | **string[]** |  | [optional]
**enabled** | **bool** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
