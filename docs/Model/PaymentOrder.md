# # PaymentOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**type** | **int** | 订单类型：1新购订单,2存续费订单,3纯扩容订单，4续费+扩容订单 | [optional]
**account** | **int** |  | [optional]
**app** | **int** |  | [optional]
**ident** | **string** | 订单号 | [optional]
**storage** | **int** | 存储空间 | [optional]
**shards** | **int** | 分片数 | [optional]
**replicas** | **int** | 副本数 | [optional]
**searchs** | **int** | 搜索次数 | [optional]
**days** | **int** | 购买的天数 | [optional]
**price** | **int** |  | [optional]
**payment** | **string** |  | [optional]
**remark** | **string** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**expired_at** | **\DateTime** |  | [optional]
**receipt** | **int** | 订单的银行回执 | [optional]
**receipt_url** | **string** | 银行回执的图片地址 | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
