# # WidgetForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  |
**type** | **string** |  | [default to 'web']
**sub_type** | **string** |  | [optional] [default to 'None']
**intro** | **string** |  | [optional]
**queries** | **int[]** |  |
**layout** | **string** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
