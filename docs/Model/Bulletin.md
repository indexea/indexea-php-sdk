# # Bulletin

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**level** | **string** |  | [optional]
**scope** | **int** |  | [optional]
**title** | **string** |  | [optional]
**content** | **string** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**start_at** | **\DateTime** |  | [optional]
**expired_at** | **\DateTime** |  | [optional]
**closable** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
