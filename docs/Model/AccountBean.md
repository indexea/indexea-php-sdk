# # AccountBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**scopes** | **string** | 如果是第三方应用，此值为三方应用的权限 | [optional]
**token** | **string** | 登录令牌(执行登录方法时该值为可用的 token) | [optional]
**token_at** | **\DateTime** | token 的有效时间 | [optional]
**name** | **string** |  | [optional]
**account** | **string** |  | [optional]
**avatar** | **string** |  | [optional]
**company** | **string** |  | [optional]
**nation** | **string** |  | [optional]
**state** | **string** |  | [optional]
**city** | **string** |  | [optional]
**address** | **string** |  | [optional]
**login_ip** | **string** |  | [optional]
**login_at** | **\DateTime** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**origin** | **string** | 账号来源 | [optional]
**subscription** | **int** |  | [optional]
**settings** | **object** |  | [optional]
**roles** | **string[]** |  | [optional]
**status** | **int** |  | [optional]
**apps** | [**\OpenAPI\Client\modals\AppBean[]**](AppBean.md) | 应用列表 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
