# # QueryBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**ident** | **string** |  | [optional]
**account** | **int** |  | [optional]
**app** | **int** |  | [optional]
**indices** | [**\OpenAPI\Client\modals\IndexBean[]**](IndexBean.md) | 关联的索引列表 | [optional]
**name** | **string** |  | [optional]
**intro** | **string** |  | [optional]
**query** | **object** |  | [optional]
**suggest** | **object** |  | [optional]
**script_score** | **object** |  | [optional]
**script_fields** | **object** |  | [optional]
**sort_fields** | [**\OpenAPI\Client\modals\QuerySortField[]**](QuerySortField.md) |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**settings** | **object** | 查询设置项 | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
