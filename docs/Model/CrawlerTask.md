# # CrawlerTask

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** |  | [optional]
**template** | **string** |  | [optional]
**period** | **int** |  | [optional]
**settings** | **object** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**run_times** | **int** |  | [optional]
**last_updated_at** | **\DateTime** |  | [optional]
**last_error** | **string** |  | [optional]
**error_count** | **int** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
