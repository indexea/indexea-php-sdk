# # KeywordBindingBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**app** | **int** |  | [optional]
**index** | **int** |  | [optional]
**query** | **int** |  | [optional]
**keyword** | **string** |  | [optional]
**alias** | **string[]** |  | [optional]
**ids** | **string[]** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
