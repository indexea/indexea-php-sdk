# # WidgetBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**ident** | **string** |  | [optional]
**account** | **int** |  | [optional]
**app** | **int** |  | [optional]
**name** | **string** |  | [optional]
**type** | **string** |  | [optional] [default to 'web']
**sub_type** | **string** |  | [optional] [default to 'None']
**intro** | **string** |  | [optional]
**queries** | [**\OpenAPI\Client\modals\QueryBean[]**](QueryBean.md) |  | [optional]
**layout** | **string** | 组件布局 | [optional]
**settings** | **object** | 组件设置参数 | [optional]
**download_url** | **string** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
