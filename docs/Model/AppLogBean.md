# # AppLogBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**app** | **int** |  | [optional]
**account** | [**\OpenAPI\Client\modals\AppLogAccount**](AppLogAccount.md) |  | [optional]
**index** | **int** |  | [optional]
**widget** | **int** |  | [optional]
**query** | **int** |  | [optional]
**recomm** | **int** |  | [optional]
**type** | **int** | 日志类型值 | [optional]
**type_desc** | **string** | 日志类型说明 | [optional]
**log** | **string** | 日志内容 | [optional]
**detail** | **object** |  | [optional]
**ip** | **string** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
