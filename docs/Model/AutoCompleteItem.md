# # AutoCompleteItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **string** |  | [optional]
**url** | **string** |  | [optional]
**props** | **object** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
