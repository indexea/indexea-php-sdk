# # QuerySortField

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **string** | 排序字段名 | [optional]
**label** | **string** | 排序字段在前端显示的名称 | [optional]
**order** | **string** | 默认排序方式 | [optional] [default to 'desc']

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
