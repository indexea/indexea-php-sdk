# # PaymentRecord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**ident** | **string** |  | [optional]
**method** | **string** |  | [optional]
**amount** | **int** |  | [optional]
**params** | **object** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
