# # QueryVariableBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**app** | **int** |  | [optional]
**name** | **string** | 变量名 | [optional]
**pname** | **string** | 变量对应 HTTP 请求的参数名 | [optional]
**intro** | **string** |  | [optional]
**type** | **string** | 变量类型 | [optional]
**required** | **bool** |  | [optional]
**format** | **string** |  | [optional]
**value** | **string** |  | [optional]
**values** | **string** | 变量可用值列表，以逗号分隔 | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
