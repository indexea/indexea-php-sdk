# # AppMentorBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | **int** | 应用编号 | [optional]
**account** | **int** | 成员编号 | [optional]
**name** | **string** | 成员备注名 | [optional]
**app_name** | **string** | 成员对应用的备注名 | [optional]
**scopes** | **string** | 成员权限 | [optional]
**report** | **int** | 是否接收报告 | [optional]
**settings** | **object** | 成员的设置项 | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
