# # RecommendBean

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 编号 | [optional]
**ident** | **string** | 推荐标识 | [optional]
**app** | **int** | 应用编号 | [optional]
**account** | **int** | 账号 | [optional]
**index** | **int** | 关联的索引编号 | [optional]
**name** | **string** | 名称 | [optional]
**intro** | **string** | 简介 | [optional]
**type** | **string** | 类型 | [optional]
**settings** | **object** | 推荐设定 | [optional]
**status** | **int** | 状态 | [optional]
**created_at** | **\DateTime** | 创建时间 | [optional]
**updated_at** | **\DateTime** | 最后更新时间 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
