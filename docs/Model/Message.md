# # Message

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**account** | **int** |  | [optional]
**sender** | **int** |  | [optional]
**receiver** | **int** |  | [optional]
**name** | **string** |  | [optional]
**avater** | **string** |  | [optional]
**msg** | **string** |  | [optional]
**type** | **int** |  | [optional]
**msgid** | **int** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**read_at** | **\DateTime** |  | [optional]
**status** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
