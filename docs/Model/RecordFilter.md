# # RecordFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filters** | **object[]** | 过滤条件 | [optional]
**aggs** | **object** | 聚合字段 | [optional]
**sorts** | **object[]** | 排序字段 | [optional]
**post_filters** | **object** | 聚合过滤条件 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
