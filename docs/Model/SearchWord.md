# # SearchWord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**q** | **string** | 搜索词 | [optional]
**count** | **int** | 该词搜索次数 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
