# # IndexTemplates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** | 总数 | [optional]
**items** | [**\OpenAPI\Client\modals\IndexTemplate[]**](IndexTemplate.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
