# OpenAPI\Client\StatsApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**statsRecomms()**](StatsApi.md#statsRecomms) | **GET** /stats/{app}/recomms | 获取推荐日志的汇总信息 |
| [**statsSearchs()**](StatsApi.md#statsSearchs) | **GET** /stats/{app}/searchs | 获取搜索日志的汇总信息 |
| [**statsTopClicks()**](StatsApi.md#statsTopClicks) | **GET** /stats/{app}/top-clicks | 获取点击排行榜 |
| [**statsWidgets()**](StatsApi.md#statsWidgets) | **GET** /stats/{app}/widgets | 获取模板与组件的统计信息 |


## `statsRecomms()`

```php
statsRecomms($app, $recomm, $start_date, $end_date, $interval): object
```

获取推荐日志的汇总信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\StatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$recomm = 0; // int | 统计指定推荐
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 统计起始日期
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 统计结束日期
$interval = 'date'; // string | 统计间隔 - 日、周、月、季度、年

try {
    $result = $apiInstance->statsRecomms($app, $recomm, $start_date, $end_date, $interval);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatsApi->statsRecomms: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **recomm** | **int**| 统计指定推荐 | [optional] [default to 0] |
| **start_date** | **\DateTime**| 统计起始日期 | [optional] |
| **end_date** | **\DateTime**| 统计结束日期 | [optional] |
| **interval** | **string**| 统计间隔 - 日、周、月、季度、年 | [optional] [default to &#39;date&#39;] |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `statsSearchs()`

```php
statsSearchs($app, $index, $query, $widget, $keywords_with_clicks, $start_date, $end_date, $interval): object
```

获取搜索日志的汇总信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\StatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 0; // int | 统计指定索引
$query = 0; // int | 统计指定查询
$widget = 0; // int | 统计指定组件
$keywords_with_clicks = true; // bool | 是否只统计有点击的关键词
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 统计起始日期
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 统计结束日期
$interval = 'date'; // string | 统计间隔 - 日、周、月、季度、年

try {
    $result = $apiInstance->statsSearchs($app, $index, $query, $widget, $keywords_with_clicks, $start_date, $end_date, $interval);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatsApi->statsSearchs: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 统计指定索引 | [optional] [default to 0] |
| **query** | **int**| 统计指定查询 | [optional] [default to 0] |
| **widget** | **int**| 统计指定组件 | [optional] [default to 0] |
| **keywords_with_clicks** | **bool**| 是否只统计有点击的关键词 | [optional] [default to true] |
| **start_date** | **\DateTime**| 统计起始日期 | [optional] |
| **end_date** | **\DateTime**| 统计结束日期 | [optional] |
| **interval** | **string**| 统计间隔 - 日、周、月、季度、年 | [optional] [default to &#39;date&#39;] |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `statsTopClicks()`

```php
statsTopClicks($app, $size, $index, $query, $recomm, $widget, $start_date, $end_date): object[]
```

获取点击排行榜

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\StatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$size = 10; // int | 排行榜大小
$index = 0; // int | 统计指定索引
$query = 0; // int | 统计指定查询
$recomm = 0; // int | 统计指定推荐组件
$widget = 0; // int | 统计指定组件
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 统计起始日期
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 统计结束日期

try {
    $result = $apiInstance->statsTopClicks($app, $size, $index, $query, $recomm, $widget, $start_date, $end_date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatsApi->statsTopClicks: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **size** | **int**| 排行榜大小 | [default to 10] |
| **index** | **int**| 统计指定索引 | [optional] [default to 0] |
| **query** | **int**| 统计指定查询 | [optional] [default to 0] |
| **recomm** | **int**| 统计指定推荐组件 | [optional] [default to 0] |
| **widget** | **int**| 统计指定组件 | [optional] [default to 0] |
| **start_date** | **\DateTime**| 统计起始日期 | [optional] |
| **end_date** | **\DateTime**| 统计结束日期 | [optional] |

### Return type

**object[]**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `statsWidgets()`

```php
statsWidgets($app, $widget, $start_date, $end_date, $interval): object
```

获取模板与组件的统计信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\StatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$widget = 0; // int | 统计指定模板或组件
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 统计起始日期
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 统计结束日期
$interval = 'date'; // string | 统计间隔 - 日、周、月、季度、年

try {
    $result = $apiInstance->statsWidgets($app, $widget, $start_date, $end_date, $interval);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatsApi->statsWidgets: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **widget** | **int**| 统计指定模板或组件 | [optional] [default to 0] |
| **start_date** | **\DateTime**| 统计起始日期 | [optional] |
| **end_date** | **\DateTime**| 统计结束日期 | [optional] |
| **interval** | **string**| 统计间隔 - 日、周、月、季度、年 | [optional] [default to &#39;date&#39;] |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
