# OpenAPI\Client\SearchApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**searchClick()**](SearchApi.md#searchClick) | **POST** /search/widget/{widget}/click | 搜索结果点击行为收集 |
| [**searchHistories()**](SearchApi.md#searchHistories) | **GET** /search/widget/{widget}/histories | 获取当前搜索用户的最新搜索记录 |
| [**searchLogs()**](SearchApi.md#searchLogs) | **GET** /apps/{app}/logs-searchs | 获取搜索日志 |
| [**searchQueryClick()**](SearchApi.md#searchQueryClick) | **POST** /search/query/{query}/click | 搜索结果点击行为收集 |
| [**searchQueryHistories()**](SearchApi.md#searchQueryHistories) | **GET** /search/query/{query}/histories | 获取当前搜索用户的最新搜索记录 |
| [**searchQueryHotWords()**](SearchApi.md#searchQueryHotWords) | **GET** /search/query/{query}/hotwords | 获取查询相关热词 |
| [**searchQueryRepeatScroll()**](SearchApi.md#searchQueryRepeatScroll) | **POST** /search/scroll/{query} | 读取滚动搜索结果 |
| [**searchQueryScroll()**](SearchApi.md#searchQueryScroll) | **GET** /search/scroll/{query} | 基于查询的滚动搜索 |
| [**searchQuerySearch()**](SearchApi.md#searchQuerySearch) | **GET** /search/query/{query} | 基于查询的公开搜索 |
| [**searchWidgetAutoComplete()**](SearchApi.md#searchWidgetAutoComplete) | **GET** /search/widget/{widget}/autocomplete | 基于组件的搜索词自动完成 |
| [**searchWidgetHotWords()**](SearchApi.md#searchWidgetHotWords) | **GET** /search/widget/{widget}/hotwords | 获取组件搜索的相关热词 |
| [**searchWidgetSearch()**](SearchApi.md#searchWidgetSearch) | **GET** /search/widget/{widget} | 基于组件的公开搜索 |


## `searchClick()`

```php
searchClick($widget, $action_id, $doc_id, $userid, $x_token): bool
```

搜索结果点击行为收集

该接口主要用于记录用户对搜索结果的点击行为

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$widget = 'widget_example'; // string | 组件唯一标识
$action_id = 'action_id_example'; // string | 对应搜索行为编号
$doc_id = 'doc_id_example'; // string | 对应索引中的内部记录编号
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权

try {
    $result = $apiInstance->searchClick($widget, $action_id, $doc_id, $userid, $x_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchClick: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **widget** | **string**| 组件唯一标识 | |
| **action_id** | **string**| 对应搜索行为编号 | |
| **doc_id** | **string**| 对应索引中的内部记录编号 | |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `searchHistories()`

```php
searchHistories($widget, $strategy, $size, $userid, $x_token, $query): string[]
```

获取当前搜索用户的最新搜索记录

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$widget = 'widget_example'; // string | 组件唯一标识
$strategy = 'popular'; // string | 搜索记录策略
$size = 10; // int | 数量
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
$x_token = 'x_token_example'; // string | 如果要使用非公开的组件，需要组件作者授权
$query = 0; // int | 指定关联查询的编号

try {
    $result = $apiInstance->searchHistories($widget, $strategy, $size, $userid, $x_token, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchHistories: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **widget** | **string**| 组件唯一标识 | |
| **strategy** | **string**| 搜索记录策略 | [default to &#39;popular&#39;] |
| **size** | **int**| 数量 | [default to 10] |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |
| **x_token** | **string**| 如果要使用非公开的组件，需要组件作者授权 | [optional] |
| **query** | **int**| 指定关联查询的编号 | [optional] [default to 0] |

### Return type

**string[]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `searchLogs()`

```php
searchLogs($app, $indices, $scope, $widget, $query, $recomm, $start_date, $end_date, $from, $size): \OpenAPI\Client\modals\QueryActionBean[]
```

获取搜索日志

该接口主要用于获取搜索明细

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$indices = array(56); // int[] | 只看指定索引
$scope = 'all'; // string | 搜索范围
$widget = 0; // int | 搜索组件
$query = 0; // int | 指定查询
$recomm = 0; // int | 推荐组件
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 统计起始日期
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | 统计结束日期
$from = 0; // int | 起始位置
$size = 50; // int | 每页记录数量

try {
    $result = $apiInstance->searchLogs($app, $indices, $scope, $widget, $query, $recomm, $start_date, $end_date, $from, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchLogs: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **indices** | [**int[]**](../Model/int.md)| 只看指定索引 | [optional] |
| **scope** | **string**| 搜索范围 | [optional] [default to &#39;all&#39;] |
| **widget** | **int**| 搜索组件 | [optional] [default to 0] |
| **query** | **int**| 指定查询 | [optional] [default to 0] |
| **recomm** | **int**| 推荐组件 | [optional] [default to 0] |
| **start_date** | **\DateTime**| 统计起始日期 | [optional] |
| **end_date** | **\DateTime**| 统计结束日期 | [optional] |
| **from** | **int**| 起始位置 | [optional] [default to 0] |
| **size** | **int**| 每页记录数量 | [optional] [default to 50] |

### Return type

[**\OpenAPI\Client\modals\QueryActionBean[]**](../Model/QueryActionBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `searchQueryClick()`

```php
searchQueryClick($query, $action_id, $doc_id, $userid, $x_token): bool
```

搜索结果点击行为收集

该接口主要用于记录用户对搜索结果的点击行为

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$query = 'query_example'; // string | 查询标识
$action_id = 'action_id_example'; // string | 对应搜索行为编号
$doc_id = 'doc_id_example'; // string | 对应索引中的内部记录编号
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权

try {
    $result = $apiInstance->searchQueryClick($query, $action_id, $doc_id, $userid, $x_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchQueryClick: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **query** | **string**| 查询标识 | |
| **action_id** | **string**| 对应搜索行为编号 | |
| **doc_id** | **string**| 对应索引中的内部记录编号 | |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `searchQueryHistories()`

```php
searchQueryHistories($query, $strategy, $size, $userid, $x_token): string[]
```

获取当前搜索用户的最新搜索记录

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$query = 'query_example'; // string | 查询唯一标识
$strategy = 'popular'; // string | 搜索记录策略
$size = 10; // int | 数量
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
$x_token = 'x_token_example'; // string | 如果要使用非公开查询，需要组件作者授权

try {
    $result = $apiInstance->searchQueryHistories($query, $strategy, $size, $userid, $x_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchQueryHistories: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **query** | **string**| 查询唯一标识 | |
| **strategy** | **string**| 搜索记录策略 | [default to &#39;popular&#39;] |
| **size** | **int**| 数量 | [default to 10] |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |
| **x_token** | **string**| 如果要使用非公开查询，需要组件作者授权 | [optional] |

### Return type

**string[]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `searchQueryHotWords()`

```php
searchQueryHotWords($query, $scope, $count, $x_token, $userid): \OpenAPI\Client\modals\SearchWord[]
```

获取查询相关热词

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$query = 'query_example'; // string | 查询标识
$scope = 'all'; // string | 时间范围
$count = 10; // int | 获取热词数量
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64

try {
    $result = $apiInstance->searchQueryHotWords($query, $scope, $count, $x_token, $userid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchQueryHotWords: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **query** | **string**| 查询标识 | |
| **scope** | **string**| 时间范围 | [default to &#39;all&#39;] |
| **count** | **int**| 获取热词数量 | [default to 10] |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |

### Return type

[**\OpenAPI\Client\modals\SearchWord[]**](../Model/SearchWord.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `searchQueryRepeatScroll()`

```php
searchQueryRepeatScroll($query, $id, $x_token, $userid): object
```

读取滚动搜索结果

先调用 search.queryScroll 获取 scroll_id 值以及第一批结果记录，然后使用 scroll_id 值调用该接口获取下一批结果记录，请注意该值的有效期是 1 分钟

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$query = 'query_example'; // string | 查询标识
$id = 'id_example'; // string | scroll_id 值，该值的有效期是 1 分钟
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64

try {
    $result = $apiInstance->searchQueryRepeatScroll($query, $id, $x_token, $userid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchQueryRepeatScroll: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **query** | **string**| 查询标识 | |
| **id** | **string**| scroll_id 值，该值的有效期是 1 分钟 | |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `searchQueryScroll()`

```php
searchQueryScroll($query, $x_token, $userid, $size, $q, $params): object
```

基于查询的滚动搜索

用于读取超过 10000 条记录的搜索结果，当需要读取大批量查询结果时请采用此接口获得 scroll_id 值，然后再用 POST 方法 queryRepeatScroll 读取剩余的其他结果，每批次的读取间隔不能超过 1 分钟

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$query = 'query_example'; // string | 查询标识
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
$size = 100; // int | 单次滚动的记录数
$q = 'q_example'; // string | 搜索关键字
$params = array('key' => 'params_example'); // array<string,string> | 聚合参数

try {
    $result = $apiInstance->searchQueryScroll($query, $x_token, $userid, $size, $q, $params);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchQueryScroll: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **query** | **string**| 查询标识 | |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |
| **size** | **int**| 单次滚动的记录数 | [optional] [default to 100] |
| **q** | **string**| 搜索关键字 | [optional] |
| **params** | [**array<string,string>**](../Model/string.md)| 聚合参数 | [optional] |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `searchQuerySearch()`

```php
searchQuerySearch($query, $from, $size, $x_token, $userid, $q, $params): object
```

基于查询的公开搜索

该接口主要用于公开搜索，如果查询是公开的就不需要授权

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$query = 'query_example'; // string | 查询标识
$from = 0; // int | 起始记录
$size = 20; // int | 每页记录数量
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
$q = 'q_example'; // string | 搜索关键字
$params = array('key' => 'params_example'); // array<string,string> | 聚合参数

try {
    $result = $apiInstance->searchQuerySearch($query, $from, $size, $x_token, $userid, $q, $params);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchQuerySearch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **query** | **string**| 查询标识 | |
| **from** | **int**| 起始记录 | [default to 0] |
| **size** | **int**| 每页记录数量 | [default to 20] |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |
| **q** | **string**| 搜索关键字 | [optional] |
| **params** | [**array<string,string>**](../Model/string.md)| 聚合参数 | [optional] |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `searchWidgetAutoComplete()`

```php
searchWidgetAutoComplete($widget, $q, $size, $userid, $x_token, $query): \OpenAPI\Client\modals\AutoCompleteItem[]
```

基于组件的搜索词自动完成

该接口主要为搜索输入框提供自动完成的功能

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$widget = 'widget_example'; // string | 组件唯一标识
$q = 'q_example'; // string | 搜索关键字
$size = 10; // int | 数量
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权
$query = 0; // int | 查询编号

try {
    $result = $apiInstance->searchWidgetAutoComplete($widget, $q, $size, $userid, $x_token, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchWidgetAutoComplete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **widget** | **string**| 组件唯一标识 | |
| **q** | **string**| 搜索关键字 | |
| **size** | **int**| 数量 | [default to 10] |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |
| **query** | **int**| 查询编号 | [optional] [default to 0] |

### Return type

[**\OpenAPI\Client\modals\AutoCompleteItem[]**](../Model/AutoCompleteItem.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `searchWidgetHotWords()`

```php
searchWidgetHotWords($widget, $x_token, $userid, $query, $scope, $count): \OpenAPI\Client\modals\SearchWord[]
```

获取组件搜索的相关热词

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$widget = 'widget_example'; // string | 组件唯一标识
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
$query = 56; // int | 查询编号
$scope = 'scope_example'; // string | 时间范围
$count = 10; // int | 获取热词数量

try {
    $result = $apiInstance->searchWidgetHotWords($widget, $x_token, $userid, $query, $scope, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchWidgetHotWords: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **widget** | **string**| 组件唯一标识 | |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |
| **query** | **int**| 查询编号 | [optional] |
| **scope** | **string**| 时间范围 | [optional] |
| **count** | **int**| 获取热词数量 | [optional] [default to 10] |

### Return type

[**\OpenAPI\Client\modals\SearchWord[]**](../Model/SearchWord.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `searchWidgetSearch()`

```php
searchWidgetSearch($widget, $from, $size, $userid, $x_token, $original, $query, $q, $params): object
```

基于组件的公开搜索

该接口主要为UI组件提供公开搜索

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$widget = 'widget_example'; // string | 组件唯一标识
$from = 0; // int | 起始记录
$size = -1; // int | 每页记录数量, 如果值小于0则使用预设值的记录数
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权
$original = 'original_example'; // string | 搜索动作的延续，在 Web 组件中一边输入即时搜索时，使用的是同一个 original，original 值等于第一个搜索动作产生结果中的 action 值
$query = 0; // int | 查询编号
$q = 'q_example'; // string | 搜索关键字
$params = array('key' => 'params_example'); // array<string,string> | 聚合参数

try {
    $result = $apiInstance->searchWidgetSearch($widget, $from, $size, $userid, $x_token, $original, $query, $q, $params);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->searchWidgetSearch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **widget** | **string**| 组件唯一标识 | |
| **from** | **int**| 起始记录 | [default to 0] |
| **size** | **int**| 每页记录数量, 如果值小于0则使用预设值的记录数 | [default to -1] |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |
| **original** | **string**| 搜索动作的延续，在 Web 组件中一边输入即时搜索时，使用的是同一个 original，original 值等于第一个搜索动作产生结果中的 action 值 | [optional] |
| **query** | **int**| 查询编号 | [optional] [default to 0] |
| **q** | **string**| 搜索关键字 | [optional] |
| **params** | [**array<string,string>**](../Model/string.md)| 聚合参数 | [optional] |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
