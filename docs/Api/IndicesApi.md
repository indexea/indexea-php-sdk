# OpenAPI\Client\IndicesApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**indexCleanup()**](IndicesApi.md#indexCleanup) | **POST** /indices/{app}/{index}/cleanup | 清空索引记录 |
| [**indexCopyTo()**](IndicesApi.md#indexCopyTo) | **POST** /indices/{app}/{index}/copyto | 导出索引数据 |
| [**indexCreate()**](IndicesApi.md#indexCreate) | **POST** /indices/{app} | 创建索引 |
| [**indexCreateTemplate()**](IndicesApi.md#indexCreateTemplate) | **POST** /indices/templates | 创建索引模板 |
| [**indexDelete()**](IndicesApi.md#indexDelete) | **DELETE** /indices/{app}/{index} | 删除索引 |
| [**indexDeleteCrawlerTask()**](IndicesApi.md#indexDeleteCrawlerTask) | **DELETE** /indices/{app}/{index}/crawler-settings | 删除索引的数据爬取任务 |
| [**indexDeleteTemplate()**](IndicesApi.md#indexDeleteTemplate) | **DELETE** /indices/templates | 删除索引模板 |
| [**indexExport()**](IndicesApi.md#indexExport) | **POST** /indices/{app}/{index}/export | 导出索引数据 |
| [**indexFlush()**](IndicesApi.md#indexFlush) | **POST** /indices/{app}/{index}/flush | 刷新索引数据，主要用于将内存中的索引数据写入磁盘 |
| [**indexFlushSettings()**](IndicesApi.md#indexFlushSettings) | **PUT** /indices/{app}/{index}/settings | 写入设置信息到索引 |
| [**indexGet()**](IndicesApi.md#indexGet) | **GET** /indices/{app}/{index} | 获取单个索引详情 |
| [**indexGetCrawlerLogs()**](IndicesApi.md#indexGetCrawlerLogs) | **GET** /indices/{app}/{index}/crawler-logs | 获取索引的爬虫任务的爬取日志 |
| [**indexGetCrawlerTask()**](IndicesApi.md#indexGetCrawlerTask) | **GET** /indices/{app}/{index}/crawler-settings | 获取索引的爬虫任务设定 |
| [**indexGetFilterSettings()**](IndicesApi.md#indexGetFilterSettings) | **GET** /indices/{app}/{index}/filter-settings | 获取索引设置信息 |
| [**indexGetSettings()**](IndicesApi.md#indexGetSettings) | **GET** /indices/{app}/{index}/settings | 获取索引设置信息 |
| [**indexList()**](IndicesApi.md#indexList) | **GET** /indices/{app} | 获取应用的索引列表 |
| [**indexListTemplates()**](IndicesApi.md#indexListTemplates) | **GET** /indices/templates | 获取所有可用的索引模板 |
| [**indexPrefetch()**](IndicesApi.md#indexPrefetch) | **GET** /indices/crawler | 获取目标网站内容预览 |
| [**indexRebuild()**](IndicesApi.md#indexRebuild) | **POST** /indices/{app}/{index}/rebuild | 重建索引数据 |
| [**indexRebuildTask()**](IndicesApi.md#indexRebuildTask) | **GET** /indices/{app}/{index}/rebuild | 获取重建索引任务的详情 |
| [**indexSubmitCrawlerTask()**](IndicesApi.md#indexSubmitCrawlerTask) | **POST** /indices/{app}/{index}/crawler-settings | 提交或者更新索引的数据爬取任务 |
| [**indexTasks()**](IndicesApi.md#indexTasks) | **GET** /indices/{app}/tasks | 获取该索引在后台的任务列表 |
| [**indexUpdate()**](IndicesApi.md#indexUpdate) | **PUT** /indices/{app}/{index} | 修改索引 |
| [**indexUpdateSettings()**](IndicesApi.md#indexUpdateSettings) | **POST** /indices/{app}/{index}/settings | 更新索引设置信息 |
| [**indexUpdateTemplate()**](IndicesApi.md#indexUpdateTemplate) | **PUT** /indices/templates | 修改索引模板 |
| [**synonymsAdd()**](IndicesApi.md#synonymsAdd) | **POST** /indices/{app}/{index}/synonyms | 添加同义词 |
| [**synonymsDelete()**](IndicesApi.md#synonymsDelete) | **DELETE** /indices/{app}/{index}/synonyms | 删除同义词 |
| [**synonymsEnable()**](IndicesApi.md#synonymsEnable) | **PATCH** /indices/{app}/{index}/synonyms | 启用禁用同义词 |
| [**synonymsFlush()**](IndicesApi.md#synonymsFlush) | **POST** /indices/{app}/{index}/synonyms-flush | 将同义词更新到搜索引擎的同义词表 |
| [**synonymsImport()**](IndicesApi.md#synonymsImport) | **POST** /indices/{app}/{index}/synonyms-import | 导入同义词 |
| [**synonymsList()**](IndicesApi.md#synonymsList) | **GET** /indices/{app}/{index}/synonyms | 获取索引的所有同义词 |
| [**synonymsUpdate()**](IndicesApi.md#synonymsUpdate) | **PUT** /indices/{app}/{index}/synonyms | 修改同义词 |


## `indexCleanup()`

```php
indexCleanup($app, $index, $vcode): object
```

清空索引记录

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$vcode = 'vcode_example'; // string | 验证码

try {
    $result = $apiInstance->indexCleanup($app, $index, $vcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexCleanup: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **vcode** | **string**| 验证码 | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexCopyTo()`

```php
indexCopyTo($app, $index, $name, $fields, $queries, $records): \OpenAPI\Client\modals\IndexBean
```

导出索引数据

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$name = 'name_example'; // string | 新索引的名称
$fields = True; // bool | 是否复制字段定义
$queries = True; // bool | 是否复制所有查询
$records = True; // bool | 是否复制所有文档

try {
    $result = $apiInstance->indexCopyTo($app, $index, $name, $fields, $queries, $records);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexCopyTo: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **name** | **string**| 新索引的名称 | |
| **fields** | **bool**| 是否复制字段定义 | |
| **queries** | **bool**| 是否复制所有查询 | |
| **records** | **bool**| 是否复制所有文档 | |

### Return type

[**\OpenAPI\Client\modals\IndexBean**](../Model/IndexBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexCreate()`

```php
indexCreate($app, $index_form): \OpenAPI\Client\modals\IndexBean
```

创建索引

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index_form = new \OpenAPI\Client\modals\IndexForm(); // \OpenAPI\Client\modals\IndexForm

try {
    $result = $apiInstance->indexCreate($app, $index_form);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexCreate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index_form** | [**\OpenAPI\Client\modals\IndexForm**](../Model/IndexForm.md)|  | |

### Return type

[**\OpenAPI\Client\modals\IndexBean**](../Model/IndexBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexCreateTemplate()`

```php
indexCreateTemplate($index_template): \OpenAPI\Client\modals\IndexTemplate
```

创建索引模板

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index_template = new \OpenAPI\Client\modals\IndexTemplate(); // \OpenAPI\Client\modals\IndexTemplate

try {
    $result = $apiInstance->indexCreateTemplate($index_template);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexCreateTemplate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **index_template** | [**\OpenAPI\Client\modals\IndexTemplate**](../Model/IndexTemplate.md)|  | |

### Return type

[**\OpenAPI\Client\modals\IndexTemplate**](../Model/IndexTemplate.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexDelete()`

```php
indexDelete($app, $index, $vcode): bool
```

删除索引

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$vcode = 'vcode_example'; // string

try {
    $result = $apiInstance->indexDelete($app, $index, $vcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **vcode** | **string**|  | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexDeleteCrawlerTask()`

```php
indexDeleteCrawlerTask($app, $index): bool
```

删除索引的数据爬取任务

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号

try {
    $result = $apiInstance->indexDeleteCrawlerTask($app, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexDeleteCrawlerTask: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexDeleteTemplate()`

```php
indexDeleteTemplate($id): bool
```

删除索引模板

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->indexDeleteTemplate($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexDeleteTemplate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexExport()`

```php
indexExport($app, $index, $format): bool
```

导出索引数据

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$format = 'json'; // string | 导出数据的格式

try {
    $result = $apiInstance->indexExport($app, $index, $format);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexExport: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **format** | **string**| 导出数据的格式 | [default to &#39;json&#39;] |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexFlush()`

```php
indexFlush($app, $index): bool
```

刷新索引数据，主要用于将内存中的索引数据写入磁盘

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号

try {
    $result = $apiInstance->indexFlush($app, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexFlush: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexFlushSettings()`

```php
indexFlushSettings($app, $index, $type, $password): object
```

写入设置信息到索引

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$type = 'type_example'; // string | 设置类型
$password = 'password_example'; // string

try {
    $result = $apiInstance->indexFlushSettings($app, $index, $type, $password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexFlushSettings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **type** | **string**| 设置类型 | |
| **password** | **string**|  | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexGet()`

```php
indexGet($app, $index): \OpenAPI\Client\modals\IndexBean
```

获取单个索引详情

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号

try {
    $result = $apiInstance->indexGet($app, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |

### Return type

[**\OpenAPI\Client\modals\IndexBean**](../Model/IndexBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexGetCrawlerLogs()`

```php
indexGetCrawlerLogs($app, $index, $from, $size): \OpenAPI\Client\modals\CrawlerLogs
```

获取索引的爬虫任务的爬取日志

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$from = 56; // int
$size = 56; // int

try {
    $result = $apiInstance->indexGetCrawlerLogs($app, $index, $from, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexGetCrawlerLogs: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **from** | **int**|  | |
| **size** | **int**|  | |

### Return type

[**\OpenAPI\Client\modals\CrawlerLogs**](../Model/CrawlerLogs.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexGetCrawlerTask()`

```php
indexGetCrawlerTask($app, $index): \OpenAPI\Client\modals\CrawlerTask
```

获取索引的爬虫任务设定

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号

try {
    $result = $apiInstance->indexGetCrawlerTask($app, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexGetCrawlerTask: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |

### Return type

[**\OpenAPI\Client\modals\CrawlerTask**](../Model/CrawlerTask.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexGetFilterSettings()`

```php
indexGetFilterSettings($app, $index, $type): string[]
```

获取索引设置信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$type = 'type_example'; // string | 设置类型

try {
    $result = $apiInstance->indexGetFilterSettings($app, $index, $type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexGetFilterSettings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **type** | **string**| 设置类型 | |

### Return type

**string[]**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexGetSettings()`

```php
indexGetSettings($app, $index, $type): \OpenAPI\Client\modals\IndexSettings
```

获取索引设置信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$type = 'type_example'; // string | 设置类型

try {
    $result = $apiInstance->indexGetSettings($app, $index, $type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexGetSettings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **type** | **string**| 设置类型 | |

### Return type

[**\OpenAPI\Client\modals\IndexSettings**](../Model/IndexSettings.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexList()`

```php
indexList($app, $stat): \OpenAPI\Client\modals\IndexBean[]
```

获取应用的索引列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$stat = true; // bool | 是否包含索引的统计信息

try {
    $result = $apiInstance->indexList($app, $stat);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexList: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **stat** | **bool**| 是否包含索引的统计信息 | [optional] [default to true] |

### Return type

[**\OpenAPI\Client\modals\IndexBean[]**](../Model/IndexBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexListTemplates()`

```php
indexListTemplates($from, $size): \OpenAPI\Client\modals\IndexTemplates
```

获取所有可用的索引模板

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$from = 0; // int
$size = 100; // int

try {
    $result = $apiInstance->indexListTemplates($from, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexListTemplates: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **from** | **int**|  | [default to 0] |
| **size** | **int**|  | [default to 100] |

### Return type

[**\OpenAPI\Client\modals\IndexTemplates**](../Model/IndexTemplates.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexPrefetch()`

```php
indexPrefetch($type, $url): object[]
```

获取目标网站内容预览

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$type = 'type_example'; // string
$url = 'url_example'; // string

try {
    $result = $apiInstance->indexPrefetch($type, $url);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexPrefetch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **type** | **string**|  | |
| **url** | **string**|  | |

### Return type

**object[]**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexRebuild()`

```php
indexRebuild($app, $index, $index_rebuild_form): \OpenAPI\Client\modals\IndexTask
```

重建索引数据

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$index_rebuild_form = new \OpenAPI\Client\modals\IndexRebuildForm(); // \OpenAPI\Client\modals\IndexRebuildForm

try {
    $result = $apiInstance->indexRebuild($app, $index, $index_rebuild_form);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexRebuild: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **index_rebuild_form** | [**\OpenAPI\Client\modals\IndexRebuildForm**](../Model/IndexRebuildForm.md)|  | |

### Return type

[**\OpenAPI\Client\modals\IndexTask**](../Model/IndexTask.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexRebuildTask()`

```php
indexRebuildTask($app, $index, $task_id): \OpenAPI\Client\modals\IndexTask
```

获取重建索引任务的详情

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$task_id = 0; // int | 任务编号,传0则获取最新的任务信息

try {
    $result = $apiInstance->indexRebuildTask($app, $index, $task_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexRebuildTask: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **task_id** | **int**| 任务编号,传0则获取最新的任务信息 | [default to 0] |

### Return type

[**\OpenAPI\Client\modals\IndexTask**](../Model/IndexTask.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexSubmitCrawlerTask()`

```php
indexSubmitCrawlerTask($app, $index, $crawler_task): \OpenAPI\Client\modals\CrawlerTask
```

提交或者更新索引的数据爬取任务

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$crawler_task = new \OpenAPI\Client\modals\CrawlerTask(); // \OpenAPI\Client\modals\CrawlerTask

try {
    $result = $apiInstance->indexSubmitCrawlerTask($app, $index, $crawler_task);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexSubmitCrawlerTask: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **crawler_task** | [**\OpenAPI\Client\modals\CrawlerTask**](../Model/CrawlerTask.md)|  | |

### Return type

[**\OpenAPI\Client\modals\CrawlerTask**](../Model/CrawlerTask.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexTasks()`

```php
indexTasks($app): array<string,\OpenAPI\Client\modals\IndexTask>
```

获取该索引在后台的任务列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->indexTasks($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexTasks: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**array<string,\OpenAPI\Client\modals\IndexTask>**](../Model/IndexTask.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexUpdate()`

```php
indexUpdate($app, $index, $index_form): \OpenAPI\Client\modals\IndexBean
```

修改索引

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$index_form = new \OpenAPI\Client\modals\IndexForm(); // \OpenAPI\Client\modals\IndexForm

try {
    $result = $apiInstance->indexUpdate($app, $index, $index_form);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexUpdate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **index_form** | [**\OpenAPI\Client\modals\IndexForm**](../Model/IndexForm.md)|  | |

### Return type

[**\OpenAPI\Client\modals\IndexBean**](../Model/IndexBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexUpdateSettings()`

```php
indexUpdateSettings($app, $index, $type, $body): bool
```

更新索引设置信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$type = 'type_example'; // string | 设置类型
$body = 'body_example'; // string

try {
    $result = $apiInstance->indexUpdateSettings($app, $index, $type, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexUpdateSettings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **type** | **string**| 设置类型 | |
| **body** | **string**|  | [optional] |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `text/plain`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexUpdateTemplate()`

```php
indexUpdateTemplate($index_template): \OpenAPI\Client\modals\IndexTemplate
```

修改索引模板

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index_template = new \OpenAPI\Client\modals\IndexTemplate(); // \OpenAPI\Client\modals\IndexTemplate

try {
    $result = $apiInstance->indexUpdateTemplate($index_template);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->indexUpdateTemplate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **index_template** | [**\OpenAPI\Client\modals\IndexTemplate**](../Model/IndexTemplate.md)|  | |

### Return type

[**\OpenAPI\Client\modals\IndexTemplate**](../Model/IndexTemplate.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `synonymsAdd()`

```php
synonymsAdd($app, $index, $synonyms_bean): \OpenAPI\Client\modals\SynonymsBean
```

添加同义词

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$synonyms_bean = new \OpenAPI\Client\modals\SynonymsBean(); // \OpenAPI\Client\modals\SynonymsBean

try {
    $result = $apiInstance->synonymsAdd($app, $index, $synonyms_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->synonymsAdd: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **synonyms_bean** | [**\OpenAPI\Client\modals\SynonymsBean**](../Model/SynonymsBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\SynonymsBean**](../Model/SynonymsBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `synonymsDelete()`

```php
synonymsDelete($app, $index, $id): bool
```

删除同义词

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$id = 56; // int | 同义词编号

try {
    $result = $apiInstance->synonymsDelete($app, $index, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->synonymsDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **id** | **int**| 同义词编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `synonymsEnable()`

```php
synonymsEnable($app, $index, $id, $enable, $all): bool
```

启用禁用同义词

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$id = 56; // int | 同义词编号
$enable = True; // bool | 是否启用
$all = True; // bool | 是否对所有索引起作用

try {
    $result = $apiInstance->synonymsEnable($app, $index, $id, $enable, $all);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->synonymsEnable: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **id** | **int**| 同义词编号 | |
| **enable** | **bool**| 是否启用 | [optional] |
| **all** | **bool**| 是否对所有索引起作用 | [optional] |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `synonymsFlush()`

```php
synonymsFlush($app, $index, $password): object
```

将同义词更新到搜索引擎的同义词表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$password = 'password_example'; // string

try {
    $result = $apiInstance->synonymsFlush($app, $index, $password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->synonymsFlush: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **password** | **string**|  | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `synonymsImport()`

```php
synonymsImport($app, $index, $action, $synonyms_bean): \OpenAPI\Client\modals\SynonymsBean[]
```

导入同义词

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$action = 56; // int | 覆盖方式
$synonyms_bean = array(new \OpenAPI\Client\modals\SynonymsBean()); // \OpenAPI\Client\modals\SynonymsBean[]

try {
    $result = $apiInstance->synonymsImport($app, $index, $action, $synonyms_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->synonymsImport: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **action** | **int**| 覆盖方式 | |
| **synonyms_bean** | [**\OpenAPI\Client\modals\SynonymsBean[]**](../Model/SynonymsBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\SynonymsBean[]**](../Model/SynonymsBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `synonymsList()`

```php
synonymsList($app, $index, $size, $type, $from): \OpenAPI\Client\modals\SynonymsBean[]
```

获取索引的所有同义词

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$size = 99999; // int
$type = 0; // int | 类型
$from = 0; // int

try {
    $result = $apiInstance->synonymsList($app, $index, $size, $type, $from);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->synonymsList: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **size** | **int**|  | [default to 99999] |
| **type** | **int**| 类型 | [optional] [default to 0] |
| **from** | **int**|  | [optional] [default to 0] |

### Return type

[**\OpenAPI\Client\modals\SynonymsBean[]**](../Model/SynonymsBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `synonymsUpdate()`

```php
synonymsUpdate($app, $index, $synonyms_bean): \OpenAPI\Client\modals\SynonymsBean
```

修改同义词

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\IndicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$synonyms_bean = new \OpenAPI\Client\modals\SynonymsBean(); // \OpenAPI\Client\modals\SynonymsBean

try {
    $result = $apiInstance->synonymsUpdate($app, $index, $synonyms_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesApi->synonymsUpdate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **synonyms_bean** | [**\OpenAPI\Client\modals\SynonymsBean**](../Model/SynonymsBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\SynonymsBean**](../Model/SynonymsBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
