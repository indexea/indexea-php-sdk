# OpenAPI\Client\PaymentApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**paymentAlipay()**](PaymentApi.md#paymentAlipay) | **POST** /payment/alipay | 接受支付宝的支付结果 |
| [**paymentAlipayReturn()**](PaymentApi.md#paymentAlipayReturn) | **GET** /payment/alipay | 支付宝平台支付完毕后调整到该接口 |
| [**paymentApplyInvoice()**](PaymentApi.md#paymentApplyInvoice) | **POST** /payment/{app}/invoices | 申请发票 |
| [**paymentBeginPay()**](PaymentApi.md#paymentBeginPay) | **POST** /payment/{app}/orders/{ident} | 支付订单 |
| [**paymentBuy()**](PaymentApi.md#paymentBuy) | **PUT** /payment/{app}/orders | 下单购买 |
| [**paymentDeleteInvoice()**](PaymentApi.md#paymentDeleteInvoice) | **DELETE** /payment/{app}/invoices | 删除发票 |
| [**paymentDeleteOrder()**](PaymentApi.md#paymentDeleteOrder) | **DELETE** /payment/{app}/orders/{ident} | 取消订单 |
| [**paymentInvoices()**](PaymentApi.md#paymentInvoices) | **GET** /payment/{app}/invoices | 获取发票列表 |
| [**paymentOrder()**](PaymentApi.md#paymentOrder) | **GET** /payment/{app}/orders/{ident} | 获取订单详情 |
| [**paymentOrders()**](PaymentApi.md#paymentOrders) | **POST** /payment/{app}/orders | 订单列表 |
| [**paymentOrdersWithoutInvoice()**](PaymentApi.md#paymentOrdersWithoutInvoice) | **GET** /payment/{app}/orders_without_invoice | 获取未曾开票的订单列表 |
| [**paymentPrice()**](PaymentApi.md#paymentPrice) | **GET** /payment/{app}/price | 获取套餐价格 |
| [**paymentReceipt()**](PaymentApi.md#paymentReceipt) | **GET** /payment/{app}/orders | 获取订单回执图片 |
| [**paymentRequestContact()**](PaymentApi.md#paymentRequestContact) | **POST** /payment/{app}/contact | 联系销售获取私有化报价 |
| [**paymentUploadReceipt()**](PaymentApi.md#paymentUploadReceipt) | **PUT** /payment/{app}/orders/{ident} | 上传转账回执 |
| [**paymentWepay()**](PaymentApi.md#paymentWepay) | **POST** /payment/wepay | 接受微信支付的支付结果 |


## `paymentAlipay()`

```php
paymentAlipay()
```

接受支付宝的支付结果

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->paymentAlipay();
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentAlipay: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentAlipayReturn()`

```php
paymentAlipayReturn()
```

支付宝平台支付完毕后调整到该接口

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->paymentAlipayReturn();
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentAlipayReturn: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentApplyInvoice()`

```php
paymentApplyInvoice($app, $type, $request_body): \OpenAPI\Client\modals\PaymentInvoice
```

申请发票

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$type = 1; // int | 发票类型
$request_body = array('request_body_example'); // string[] | 申请开发票的订单编号列表

try {
    $result = $apiInstance->paymentApplyInvoice($app, $type, $request_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentApplyInvoice: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **type** | **int**| 发票类型 | [default to 1] |
| **request_body** | [**string[]**](../Model/string.md)| 申请开发票的订单编号列表 | |

### Return type

[**\OpenAPI\Client\modals\PaymentInvoice**](../Model/PaymentInvoice.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentBeginPay()`

```php
paymentBeginPay($app, $ident, $type): \OpenAPI\Client\modals\PayResult
```

支付订单

选择支付方式，开始支付

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$ident = 'ident_example'; // string | 订单号
$type = 'type_example'; // string | 支付方式

try {
    $result = $apiInstance->paymentBeginPay($app, $ident, $type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentBeginPay: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **ident** | **string**| 订单号 | |
| **type** | **string**| 支付方式 | |

### Return type

[**\OpenAPI\Client\modals\PayResult**](../Model/PayResult.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentBuy()`

```php
paymentBuy($app, $payment_service): \OpenAPI\Client\modals\PaymentOrder
```

下单购买



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$payment_service = new \OpenAPI\Client\modals\PaymentService(); // \OpenAPI\Client\modals\PaymentService

try {
    $result = $apiInstance->paymentBuy($app, $payment_service);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentBuy: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **payment_service** | [**\OpenAPI\Client\modals\PaymentService**](../Model/PaymentService.md)|  | |

### Return type

[**\OpenAPI\Client\modals\PaymentOrder**](../Model/PaymentOrder.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentDeleteInvoice()`

```php
paymentDeleteInvoice($app, $id): bool
```

删除发票

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$id = 56; // int | 发票编号

try {
    $result = $apiInstance->paymentDeleteInvoice($app, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentDeleteInvoice: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **id** | **int**| 发票编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentDeleteOrder()`

```php
paymentDeleteOrder($app, $ident): bool
```

取消订单

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$ident = 'ident_example'; // string | 订单号

try {
    $result = $apiInstance->paymentDeleteOrder($app, $ident);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentDeleteOrder: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **ident** | **string**| 订单号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentInvoices()`

```php
paymentInvoices($app): \OpenAPI\Client\modals\PaymentInvoice[]
```

获取发票列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->paymentInvoices($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentInvoices: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\PaymentInvoice[]**](../Model/PaymentInvoice.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentOrder()`

```php
paymentOrder($app, $ident): \OpenAPI\Client\modals\PaymentOrder
```

获取订单详情

获取订单详情

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$ident = 'ident_example'; // string | 订单号

try {
    $result = $apiInstance->paymentOrder($app, $ident);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentOrder: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **ident** | **string**| 订单号 | |

### Return type

[**\OpenAPI\Client\modals\PaymentOrder**](../Model/PaymentOrder.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentOrders()`

```php
paymentOrders($app): \OpenAPI\Client\modals\PaymentOrder[]
```

订单列表

获取应用的订单列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->paymentOrders($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentOrders: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\PaymentOrder[]**](../Model/PaymentOrder.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentOrdersWithoutInvoice()`

```php
paymentOrdersWithoutInvoice($app): \OpenAPI\Client\modals\PaymentOrder[]
```

获取未曾开票的订单列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->paymentOrdersWithoutInvoice($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentOrdersWithoutInvoice: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\PaymentOrder[]**](../Model/PaymentOrder.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentPrice()`

```php
paymentPrice($app, $service): \OpenAPI\Client\modals\PaymentService
```

获取套餐价格



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$service = new \OpenAPI\Client\modals\PaymentService(); // PaymentService | 配额信息

try {
    $result = $apiInstance->paymentPrice($app, $service);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentPrice: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **service** | [**PaymentService**](../Model/.md)| 配额信息 | |

### Return type

[**\OpenAPI\Client\modals\PaymentService**](../Model/PaymentService.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentReceipt()`

```php
paymentReceipt($app, $ident, $id): \SplFileObject
```

获取订单回执图片

查看回执图片

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$ident = 'ident_example'; // string | 订单号
$id = 56; // int | 回执编号

try {
    $result = $apiInstance->paymentReceipt($app, $ident, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentReceipt: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **ident** | **string**| 订单号 | |
| **id** | **int**| 回执编号 | |

### Return type

**\SplFileObject**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `default`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentRequestContact()`

```php
paymentRequestContact($app, $contact_form): bool
```

联系销售获取私有化报价

获取私有化方案、报价等信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$contact_form = new \OpenAPI\Client\modals\ContactForm(); // \OpenAPI\Client\modals\ContactForm | 客户联系信息

try {
    $result = $apiInstance->paymentRequestContact($app, $contact_form);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentRequestContact: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **contact_form** | [**\OpenAPI\Client\modals\ContactForm**](../Model/ContactForm.md)| 客户联系信息 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentUploadReceipt()`

```php
paymentUploadReceipt($app, $ident, $receipt): \OpenAPI\Client\modals\PaymentOrder
```

上传转账回执

用户在完成银行转账后，通过该接口上传转账回执

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$ident = 'ident_example'; // string | 订单号
$receipt = "/path/to/file.txt"; // \SplFileObject

try {
    $result = $apiInstance->paymentUploadReceipt($app, $ident, $receipt);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentUploadReceipt: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **ident** | **string**| 订单号 | |
| **receipt** | **\SplFileObject****\SplFileObject**|  | [optional] |

### Return type

[**\OpenAPI\Client\modals\PaymentOrder**](../Model/PaymentOrder.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentWepay()`

```php
paymentWepay()
```

接受微信支付的支付结果

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\PaymentApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->paymentWepay();
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentWepay: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
