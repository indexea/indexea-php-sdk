# OpenAPI\Client\MessageApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**messageDelete()**](MessageApi.md#messageDelete) | **DELETE** /accounts/message | 删除消息 |
| [**messageFeedback()**](MessageApi.md#messageFeedback) | **POST** /accounts/feedback | 反馈意见 |
| [**messageList()**](MessageApi.md#messageList) | **GET** /accounts/message | 获取我相关的消息信息，包括未读消息数量、最新消息等 |
| [**messageRead()**](MessageApi.md#messageRead) | **PATCH** /accounts/message | 标识消息为已读 |
| [**messageSend()**](MessageApi.md#messageSend) | **POST** /accounts/message | 发送消息 |


## `messageDelete()`

```php
messageDelete($id): bool
```

删除消息

删除消息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\MessageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | 消息编号

try {
    $result = $apiInstance->messageDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MessageApi->messageDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| 消息编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `messageFeedback()`

```php
messageFeedback($content, $type): bool
```

反馈意见

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\MessageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$content = 'content_example'; // string
$type = 'general'; // string

try {
    $result = $apiInstance->messageFeedback($content, $type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MessageApi->messageFeedback: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **content** | **string**|  | |
| **type** | **string**|  | [default to &#39;general&#39;] |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `messageList()`

```php
messageList($scope, $size, $from): \OpenAPI\Client\modals\Messages
```

获取我相关的消息信息，包括未读消息数量、最新消息等

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\MessageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$scope = 'unread'; // string
$size = 10; // int | 消息数量
$from = 0; // int | 用于翻页的起始位置

try {
    $result = $apiInstance->messageList($scope, $size, $from);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MessageApi->messageList: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **scope** | **string**|  | [default to &#39;unread&#39;] |
| **size** | **int**| 消息数量 | [default to 10] |
| **from** | **int**| 用于翻页的起始位置 | [optional] [default to 0] |

### Return type

[**\OpenAPI\Client\modals\Messages**](../Model/Messages.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `messageRead()`

```php
messageRead($id): \OpenAPI\Client\modals\Messages
```

标识消息为已读

标识消息为已读

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\MessageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | 消息编号,多个消息使用逗号隔开

try {
    $result = $apiInstance->messageRead($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MessageApi->messageRead: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **string**| 消息编号,多个消息使用逗号隔开 | |

### Return type

[**\OpenAPI\Client\modals\Messages**](../Model/Messages.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `messageSend()`

```php
messageSend($receiver, $msg): \OpenAPI\Client\modals\Message
```

发送消息

发送站内消息给某人

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\MessageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$receiver = 56; // int
$msg = 'msg_example'; // string

try {
    $result = $apiInstance->messageSend($receiver, $msg);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MessageApi->messageSend: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **receiver** | **int**|  | |
| **msg** | **string**|  | |

### Return type

[**\OpenAPI\Client\modals\Message**](../Model/Message.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
