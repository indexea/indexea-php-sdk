# OpenAPI\Client\AppsApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**appAddMentor()**](AppsApi.md#appAddMentor) | **POST** /apps/{app}/mentors | 添加应用成员 |
| [**appBlacklist()**](AppsApi.md#appBlacklist) | **GET** /apps/{app}/blacklist | 获取黑名单信息 |
| [**appCreate()**](AppsApi.md#appCreate) | **POST** /apps | 创建应用 |
| [**appCreateAccessToken()**](AppsApi.md#appCreateAccessToken) | **POST** /apps/{app}/tokens | 创建 Access Token |
| [**appCreateOauthApp()**](AppsApi.md#appCreateOauthApp) | **POST** /apps/{app}/oauth | 创建第三方应用 |
| [**appDelete()**](AppsApi.md#appDelete) | **DELETE** /apps/{app} | 删除应用 |
| [**appDeleteAccessToken()**](AppsApi.md#appDeleteAccessToken) | **DELETE** /apps/{app}/tokens | 删除 Access Token |
| [**appDeleteMentor()**](AppsApi.md#appDeleteMentor) | **DELETE** /apps/{app}/mentors | 删除应用成员 |
| [**appDeleteOauthApp()**](AppsApi.md#appDeleteOauthApp) | **DELETE** /apps/{app}/oauth | 删除第三方应用 |
| [**appExcelOfLogs()**](AppsApi.md#appExcelOfLogs) | **GET** /apps/{app}/logs | 导出应用日志到 Excel |
| [**appGet()**](AppsApi.md#appGet) | **GET** /apps/{app} | 获取应用详情 |
| [**appGetCompany()**](AppsApi.md#appGetCompany) | **POST** /apps/{app}/company | 获取应用填写的公司信息 |
| [**appGetCompanyPic()**](AppsApi.md#appGetCompanyPic) | **GET** /apps/{app}/company | 获取公司营业执照或者一般纳税人证明 |
| [**appList()**](AppsApi.md#appList) | **GET** /apps | 获取应用列表 |
| [**appListMentors()**](AppsApi.md#appListMentors) | **GET** /apps/{app}/mentors | 获取应用成员列表 |
| [**appListOauthApps()**](AppsApi.md#appListOauthApps) | **GET** /apps/{app}/oauth | 获取第三方应用列表 |
| [**appLogs()**](AppsApi.md#appLogs) | **POST** /apps/{app}/logs | 获取应用的日志列表 |
| [**appResetAccessToken()**](AppsApi.md#appResetAccessToken) | **PATCH** /apps/{app}/tokens | 重置 Access Token |
| [**appResetOauthAppSecret()**](AppsApi.md#appResetOauthAppSecret) | **POST** /apps/{app}/oauth-reset-secret | 重新生成三方应用的密钥 |
| [**appSaveBlacklist()**](AppsApi.md#appSaveBlacklist) | **PUT** /apps/{app}/blacklist | 修改应用的黑名单信息 |
| [**appSaveCompany()**](AppsApi.md#appSaveCompany) | **PUT** /apps/{app}/company | 修改应用的公司信息 |
| [**appSearchsEstimate()**](AppsApi.md#appSearchsEstimate) | **GET** /apps/{app}/searchs-estimate | 获取搜索流量包使用配额信息 |
| [**appSetTrigger()**](AppsApi.md#appSetTrigger) | **PUT** /apps/{app}/trigger | 修改应用的触发器信息 |
| [**appTokens()**](AppsApi.md#appTokens) | **GET** /apps/{app}/tokens | 获取 Access Token 列表 |
| [**appTransfer()**](AppsApi.md#appTransfer) | **POST** /apps/{app}/transfer | 转让应用给他人 |
| [**appTrigger()**](AppsApi.md#appTrigger) | **GET** /apps/{app}/trigger | 获取应用触发器详情 |
| [**appTriggerLogs()**](AppsApi.md#appTriggerLogs) | **GET** /apps/{app}/trigger-logs | 获取应用触发日志列表 |
| [**appUpdate()**](AppsApi.md#appUpdate) | **PUT** /apps/{app} | 修改应用的基本信息 |
| [**appUpdateAccessToken()**](AppsApi.md#appUpdateAccessToken) | **PUT** /apps/{app}/tokens | 修改 Access Token |
| [**appUpdateMentor()**](AppsApi.md#appUpdateMentor) | **PATCH** /apps/{app}/mentors | 修改成员备注和权限 |
| [**appUpdateMentorOptions()**](AppsApi.md#appUpdateMentorOptions) | **POST** /apps/{app}/mentors-options | 修改应用成员自身的设置（包括应用名备注，是否接收报告等） |
| [**appUpdateMentorReportOptions()**](AppsApi.md#appUpdateMentorReportOptions) | **PATCH** /apps/{app}/mentors-options | 修改应用成员自身的通知设置 |
| [**appUpdateOauthApp()**](AppsApi.md#appUpdateOauthApp) | **PATCH** /apps/{app}/oauth | 修改第三方应用信息 |
| [**appUpdateOauthAppLogo()**](AppsApi.md#appUpdateOauthAppLogo) | **PUT** /apps/{app}/oauth | 修改三方应用图标 |
| [**appUpdateStatus()**](AppsApi.md#appUpdateStatus) | **PATCH** /apps/{app} | 修改应用的状态 |


## `appAddMentor()`

```php
appAddMentor($app, $account, $scopes, $name): \OpenAPI\Client\modals\MentorForm
```

添加应用成员



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$account = 'account_example'; // string | 成员账号
$scopes = 'scopes_example'; // string | 权限
$name = 'name_example'; // string | 备注名称

try {
    $result = $apiInstance->appAddMentor($app, $account, $scopes, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appAddMentor: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **account** | **string**| 成员账号 | |
| **scopes** | **string**| 权限 | |
| **name** | **string**| 备注名称 | [optional] |

### Return type

[**\OpenAPI\Client\modals\MentorForm**](../Model/MentorForm.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appBlacklist()`

```php
appBlacklist($app): \OpenAPI\Client\modals\BlacklistBean
```

获取黑名单信息



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->appBlacklist($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appBlacklist: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\BlacklistBean**](../Model/BlacklistBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appCreate()`

```php
appCreate($name, $intro): \OpenAPI\Client\modals\AppBean
```

创建应用



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = 'name_example'; // string
$intro = 'intro_example'; // string

try {
    $result = $apiInstance->appCreate($name, $intro);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appCreate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **name** | **string**|  | |
| **intro** | **string**|  | [optional] |

### Return type

[**\OpenAPI\Client\modals\AppBean**](../Model/AppBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appCreateAccessToken()`

```php
appCreateAccessToken($app, $token_bean): \OpenAPI\Client\modals\TokenBean
```

创建 Access Token



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$token_bean = new \OpenAPI\Client\modals\TokenBean(); // \OpenAPI\Client\modals\TokenBean

try {
    $result = $apiInstance->appCreateAccessToken($app, $token_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appCreateAccessToken: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **token_bean** | [**\OpenAPI\Client\modals\TokenBean**](../Model/TokenBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\TokenBean**](../Model/TokenBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appCreateOauthApp()`

```php
appCreateOauthApp($app, $oauth_app_bean): \OpenAPI\Client\modals\OauthAppBean
```

创建第三方应用



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$oauth_app_bean = new \OpenAPI\Client\modals\OauthAppBean(); // \OpenAPI\Client\modals\OauthAppBean

try {
    $result = $apiInstance->appCreateOauthApp($app, $oauth_app_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appCreateOauthApp: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **oauth_app_bean** | [**\OpenAPI\Client\modals\OauthAppBean**](../Model/OauthAppBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\OauthAppBean**](../Model/OauthAppBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appDelete()`

```php
appDelete($app): bool
```

删除应用



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->appDelete($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appDeleteAccessToken()`

```php
appDeleteAccessToken($app, $id, $vcode): bool
```

删除 Access Token



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$id = 56; // int | Access Token 编号
$vcode = 'vcode_example'; // string

try {
    $result = $apiInstance->appDeleteAccessToken($app, $id, $vcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appDeleteAccessToken: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **id** | **int**| Access Token 编号 | |
| **vcode** | **string**|  | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appDeleteMentor()`

```php
appDeleteMentor($app, $account): bool
```

删除应用成员



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$account = 56; // int | 成员编号

try {
    $result = $apiInstance->appDeleteMentor($app, $account);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appDeleteMentor: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **account** | **int**| 成员编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appDeleteOauthApp()`

```php
appDeleteOauthApp($app, $ident, $vcode): bool
```

删除第三方应用



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$ident = 'ident_example'; // string | 三方应用编号
$vcode = 'vcode_example'; // string

try {
    $result = $apiInstance->appDeleteOauthApp($app, $ident, $vcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appDeleteOauthApp: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **ident** | **string**| 三方应用编号 | |
| **vcode** | **string**|  | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appExcelOfLogs()`

```php
appExcelOfLogs($app, $account, $indices, $widget, $query, $type, $start_date, $end_date): \SplFileObject
```

导出应用日志到 Excel

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$account = 0; // int | 账号
$indices = array(56); // int[] | 索引
$widget = 0; // int | 组件
$query = 0; // int | 查询
$type = 0; // int | 类型
$start_date = Wed Jun 09 08:00:00 CST 2021; // \DateTime | 起始日期
$end_date = Thu Aug 19 08:00:00 CST 2021; // \DateTime | 结束日期

try {
    $result = $apiInstance->appExcelOfLogs($app, $account, $indices, $widget, $query, $type, $start_date, $end_date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appExcelOfLogs: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **account** | **int**| 账号 | [optional] [default to 0] |
| **indices** | [**int[]**](../Model/int.md)| 索引 | [optional] |
| **widget** | **int**| 组件 | [optional] [default to 0] |
| **query** | **int**| 查询 | [optional] [default to 0] |
| **type** | **int**| 类型 | [optional] [default to 0] |
| **start_date** | **\DateTime**| 起始日期 | [optional] |
| **end_date** | **\DateTime**| 结束日期 | [optional] |

### Return type

**\SplFileObject**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appGet()`

```php
appGet($app): \OpenAPI\Client\modals\AppBean
```

获取应用详情



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->appGet($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\AppBean**](../Model/AppBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appGetCompany()`

```php
appGetCompany($app): \OpenAPI\Client\modals\CompanyBean
```

获取应用填写的公司信息



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->appGetCompany($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appGetCompany: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\CompanyBean**](../Model/CompanyBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appGetCompanyPic()`

```php
appGetCompanyPic($app, $type): \SplFileObject
```

获取公司营业执照或者一般纳税人证明



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$type = 'type_example'; // string | 获取图片的类型 [license,certificate]

try {
    $result = $apiInstance->appGetCompanyPic($app, $type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appGetCompanyPic: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **type** | **string**| 获取图片的类型 [license,certificate] | |

### Return type

**\SplFileObject**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `default`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appList()`

```php
appList(): \OpenAPI\Client\modals\AppBean[]
```

获取应用列表



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->appList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appList: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenAPI\Client\modals\AppBean[]**](../Model/AppBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appListMentors()`

```php
appListMentors($app, $from, $size): \OpenAPI\Client\modals\MentorForm
```

获取应用成员列表



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$from = 0; // int | 开始位置
$size = 20; // int | 获取的数量

try {
    $result = $apiInstance->appListMentors($app, $from, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appListMentors: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **from** | **int**| 开始位置 | [optional] [default to 0] |
| **size** | **int**| 获取的数量 | [optional] [default to 20] |

### Return type

[**\OpenAPI\Client\modals\MentorForm**](../Model/MentorForm.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appListOauthApps()`

```php
appListOauthApps($app): \OpenAPI\Client\modals\OauthAppBean[]
```

获取第三方应用列表



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->appListOauthApps($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appListOauthApps: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\OauthAppBean[]**](../Model/OauthAppBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appLogs()`

```php
appLogs($app, $account, $indices, $widget, $query, $type, $start_date, $end_date, $from, $size): \OpenAPI\Client\modals\AppLogsBean
```

获取应用的日志列表



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$account = 0; // int | 账号
$indices = array(56); // int[] | 索引
$widget = 0; // int | 组件
$query = 0; // int | 查询
$type = 0; // int | 类型
$start_date = Wed Jun 09 08:00:00 CST 2021; // \DateTime | 起始日期
$end_date = Thu Aug 19 08:00:00 CST 2021; // \DateTime | 结束日期
$from = 0; // int | 起始位置
$size = 20; // int | 数量

try {
    $result = $apiInstance->appLogs($app, $account, $indices, $widget, $query, $type, $start_date, $end_date, $from, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appLogs: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **account** | **int**| 账号 | [optional] [default to 0] |
| **indices** | [**int[]**](../Model/int.md)| 索引 | [optional] |
| **widget** | **int**| 组件 | [optional] [default to 0] |
| **query** | **int**| 查询 | [optional] [default to 0] |
| **type** | **int**| 类型 | [optional] [default to 0] |
| **start_date** | **\DateTime**| 起始日期 | [optional] |
| **end_date** | **\DateTime**| 结束日期 | [optional] |
| **from** | **int**| 起始位置 | [optional] [default to 0] |
| **size** | **int**| 数量 | [optional] [default to 20] |

### Return type

[**\OpenAPI\Client\modals\AppLogsBean**](../Model/AppLogsBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appResetAccessToken()`

```php
appResetAccessToken($app, $id, $vcode): \OpenAPI\Client\modals\TokenBean
```

重置 Access Token



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$id = 56; // int | Access Token 编号
$vcode = 'vcode_example'; // string

try {
    $result = $apiInstance->appResetAccessToken($app, $id, $vcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appResetAccessToken: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **id** | **int**| Access Token 编号 | |
| **vcode** | **string**|  | |

### Return type

[**\OpenAPI\Client\modals\TokenBean**](../Model/TokenBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appResetOauthAppSecret()`

```php
appResetOauthAppSecret($app, $ident, $vcode): \OpenAPI\Client\modals\OauthAppBean
```

重新生成三方应用的密钥



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$ident = 'ident_example'; // string | 三方应用标识
$vcode = 'vcode_example'; // string

try {
    $result = $apiInstance->appResetOauthAppSecret($app, $ident, $vcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appResetOauthAppSecret: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **ident** | **string**| 三方应用标识 | |
| **vcode** | **string**|  | |

### Return type

[**\OpenAPI\Client\modals\OauthAppBean**](../Model/OauthAppBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appSaveBlacklist()`

```php
appSaveBlacklist($app, $blacklist_bean): bool
```

修改应用的黑名单信息



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$blacklist_bean = new \OpenAPI\Client\modals\BlacklistBean(); // \OpenAPI\Client\modals\BlacklistBean

try {
    $result = $apiInstance->appSaveBlacklist($app, $blacklist_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appSaveBlacklist: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **blacklist_bean** | [**\OpenAPI\Client\modals\BlacklistBean**](../Model/BlacklistBean.md)|  | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appSaveCompany()`

```php
appSaveCompany($app, $name, $url, $nation, $province, $city, $taxpayer, $bank, $account, $address, $tel, $license, $certificate, $post_addr, $post_code, $post_name, $post_tel): \OpenAPI\Client\modals\CompanyBean
```

修改应用的公司信息



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$name = 'name_example'; // string | 公司名称
$url = 'url_example'; // string | 公司网址
$nation = 'nation_example'; // string | 国家
$province = 'province_example'; // string | 省/州/地区
$city = 'city_example'; // string | 城市
$taxpayer = 'taxpayer_example'; // string | 纳税人识别号
$bank = 'bank_example'; // string | 开户行
$account = 'account_example'; // string | 银行账号
$address = 'address_example'; // string | 公司注册地址
$tel = 'tel_example'; // string | 公司电话
$license = "/path/to/file.txt"; // \SplFileObject | 营业执照
$certificate = "/path/to/file.txt"; // \SplFileObject | 一般纳税人证明扫描件
$post_addr = 'post_addr_example'; // string | 快递地址
$post_code = 'post_code_example'; // string | 邮编
$post_name = 'post_name_example'; // string | 收件人姓名
$post_tel = 'post_tel_example'; // string | 收件人电话

try {
    $result = $apiInstance->appSaveCompany($app, $name, $url, $nation, $province, $city, $taxpayer, $bank, $account, $address, $tel, $license, $certificate, $post_addr, $post_code, $post_name, $post_tel);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appSaveCompany: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **name** | **string**| 公司名称 | [optional] |
| **url** | **string**| 公司网址 | [optional] |
| **nation** | **string**| 国家 | [optional] |
| **province** | **string**| 省/州/地区 | [optional] |
| **city** | **string**| 城市 | [optional] |
| **taxpayer** | **string**| 纳税人识别号 | [optional] |
| **bank** | **string**| 开户行 | [optional] |
| **account** | **string**| 银行账号 | [optional] |
| **address** | **string**| 公司注册地址 | [optional] |
| **tel** | **string**| 公司电话 | [optional] |
| **license** | **\SplFileObject****\SplFileObject**| 营业执照 | [optional] |
| **certificate** | **\SplFileObject****\SplFileObject**| 一般纳税人证明扫描件 | [optional] |
| **post_addr** | **string**| 快递地址 | [optional] |
| **post_code** | **string**| 邮编 | [optional] |
| **post_name** | **string**| 收件人姓名 | [optional] |
| **post_tel** | **string**| 收件人电话 | [optional] |

### Return type

[**\OpenAPI\Client\modals\CompanyBean**](../Model/CompanyBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appSearchsEstimate()`

```php
appSearchsEstimate($app, $days): \OpenAPI\Client\modals\SearchEstimateResult
```

获取搜索流量包使用配额信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$days = 30; // int | 计算平均搜索数的最近天数

try {
    $result = $apiInstance->appSearchsEstimate($app, $days);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appSearchsEstimate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **days** | **int**| 计算平均搜索数的最近天数 | [default to 30] |

### Return type

[**\OpenAPI\Client\modals\SearchEstimateResult**](../Model/SearchEstimateResult.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appSetTrigger()`

```php
appSetTrigger($app, $trigger_bean): bool
```

修改应用的触发器信息



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$trigger_bean = new \OpenAPI\Client\modals\TriggerBean(); // \OpenAPI\Client\modals\TriggerBean

try {
    $result = $apiInstance->appSetTrigger($app, $trigger_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appSetTrigger: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **trigger_bean** | [**\OpenAPI\Client\modals\TriggerBean**](../Model/TriggerBean.md)|  | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appTokens()`

```php
appTokens($app): \OpenAPI\Client\modals\TokenBean[]
```

获取 Access Token 列表



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->appTokens($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appTokens: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\TokenBean[]**](../Model/TokenBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appTransfer()`

```php
appTransfer($app, $vcode, $account): bool
```

转让应用给他人



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$vcode = 'vcode_example'; // string | 验证码
$account = 56; // int | 目标账号

try {
    $result = $apiInstance->appTransfer($app, $vcode, $account);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appTransfer: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **vcode** | **string**| 验证码 | |
| **account** | **int**| 目标账号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appTrigger()`

```php
appTrigger($app): \OpenAPI\Client\modals\TriggerBean
```

获取应用触发器详情

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->appTrigger($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appTrigger: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\TriggerBean**](../Model/TriggerBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appTriggerLogs()`

```php
appTriggerLogs($app, $id, $size): \OpenAPI\Client\modals\TriggerLogBean[]
```

获取应用触发日志列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$id = 999999999; // int | 起始日志编号
$size = 20; // int | 日志数

try {
    $result = $apiInstance->appTriggerLogs($app, $id, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appTriggerLogs: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **id** | **int**| 起始日志编号 | [default to 999999999] |
| **size** | **int**| 日志数 | [default to 20] |

### Return type

[**\OpenAPI\Client\modals\TriggerLogBean[]**](../Model/TriggerLogBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appUpdate()`

```php
appUpdate($app, $name, $intro): \OpenAPI\Client\modals\AppBean
```

修改应用的基本信息



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$name = 'name_example'; // string
$intro = 'intro_example'; // string

try {
    $result = $apiInstance->appUpdate($app, $name, $intro);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appUpdate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **name** | **string**|  | [optional] |
| **intro** | **string**|  | [optional] |

### Return type

[**\OpenAPI\Client\modals\AppBean**](../Model/AppBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appUpdateAccessToken()`

```php
appUpdateAccessToken($app, $token_bean): \OpenAPI\Client\modals\TokenBean
```

修改 Access Token



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$token_bean = new \OpenAPI\Client\modals\TokenBean(); // \OpenAPI\Client\modals\TokenBean

try {
    $result = $apiInstance->appUpdateAccessToken($app, $token_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appUpdateAccessToken: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **token_bean** | [**\OpenAPI\Client\modals\TokenBean**](../Model/TokenBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\TokenBean**](../Model/TokenBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appUpdateMentor()`

```php
appUpdateMentor($app, $account, $name, $scopes): bool
```

修改成员备注和权限



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$account = 56; // int | 成员编号
$name = 'name_example'; // string | 备注名称
$scopes = 'scopes_example'; // string | 权限

try {
    $result = $apiInstance->appUpdateMentor($app, $account, $name, $scopes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appUpdateMentor: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **account** | **int**| 成员编号 | |
| **name** | **string**| 备注名称 | |
| **scopes** | **string**| 权限 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appUpdateMentorOptions()`

```php
appUpdateMentorOptions($app, $name, $report): bool
```

修改应用成员自身的设置（包括应用名备注，是否接收报告等）



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$name = 'name_example'; // string | 应用备注名称，如果不填写则使用默认名称
$report = false; // bool | 是否接收使用情况报告

try {
    $result = $apiInstance->appUpdateMentorOptions($app, $name, $report);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appUpdateMentorOptions: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **name** | **string**| 应用备注名称，如果不填写则使用默认名称 | [optional] |
| **report** | **bool**| 是否接收使用情况报告 | [optional] [default to false] |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appUpdateMentorReportOptions()`

```php
appUpdateMentorReportOptions($app, $key, $type, $value, $vcode): bool
```

修改应用成员自身的通知设置



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$key = 'key_example'; // string | 配置项名称
$type = 'type_example'; // string | 配置值类型
$value = 'value_example'; // string | 配置值
$vcode = 'vcode_example'; // string | 验证码(非必须，只有在配置通知邮箱和手机的时候才需要)

try {
    $result = $apiInstance->appUpdateMentorReportOptions($app, $key, $type, $value, $vcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appUpdateMentorReportOptions: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **key** | **string**| 配置项名称 | |
| **type** | **string**| 配置值类型 | |
| **value** | **string**| 配置值 | |
| **vcode** | **string**| 验证码(非必须，只有在配置通知邮箱和手机的时候才需要) | [optional] |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appUpdateOauthApp()`

```php
appUpdateOauthApp($app, $oauth_app_bean): \OpenAPI\Client\modals\OauthAppBean
```

修改第三方应用信息



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$oauth_app_bean = new \OpenAPI\Client\modals\OauthAppBean(); // \OpenAPI\Client\modals\OauthAppBean

try {
    $result = $apiInstance->appUpdateOauthApp($app, $oauth_app_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appUpdateOauthApp: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **oauth_app_bean** | [**\OpenAPI\Client\modals\OauthAppBean**](../Model/OauthAppBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\OauthAppBean**](../Model/OauthAppBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appUpdateOauthAppLogo()`

```php
appUpdateOauthAppLogo($app, $ident, $logo): \OpenAPI\Client\modals\OauthAppBean
```

修改三方应用图标



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$ident = 'ident_example'; // string | 三方应用标识
$logo = "/path/to/file.txt"; // \SplFileObject

try {
    $result = $apiInstance->appUpdateOauthAppLogo($app, $ident, $logo);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appUpdateOauthAppLogo: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **ident** | **string**| 三方应用标识 | |
| **logo** | **\SplFileObject****\SplFileObject**|  | [optional] |

### Return type

[**\OpenAPI\Client\modals\OauthAppBean**](../Model/OauthAppBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `appUpdateStatus()`

```php
appUpdateStatus($app, $vcode, $status): bool
```

修改应用的状态



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AppsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$vcode = 'vcode_example'; // string | 验证码
$status = 56; // int | 新状态

try {
    $result = $apiInstance->appUpdateStatus($app, $vcode, $status);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AppsApi->appUpdateStatus: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **vcode** | **string**| 验证码 | |
| **status** | **int**| 新状态 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
