# OpenAPI\Client\QueriesApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**queryAnalyze()**](QueriesApi.md#queryAnalyze) | **POST** /queries/{app}/analyze | 分词测试 |
| [**queryCopy()**](QueriesApi.md#queryCopy) | **POST** /queries/{app}/copy | 复制指定查询并创建新查询 |
| [**queryCopyToQuery()**](QueriesApi.md#queryCopyToQuery) | **PUT** /queries/{app}/copy | 复制查询到已有查询 |
| [**queryCreate()**](QueriesApi.md#queryCreate) | **POST** /queries/{app} | 创建搜索 |
| [**queryCreateKeywordBindings()**](QueriesApi.md#queryCreateKeywordBindings) | **POST** /queries/{app}/keyword-bindings | 创建新的关键词文档绑定 |
| [**queryCreateVariable()**](QueriesApi.md#queryCreateVariable) | **POST** /queries/{app}/variables | 创建新的预定义查询变量 |
| [**queryDelete()**](QueriesApi.md#queryDelete) | **DELETE** /queries/{app} | 删除搜索 |
| [**queryDeleteKeywordBindings()**](QueriesApi.md#queryDeleteKeywordBindings) | **DELETE** /queries/{app}/keyword-bindings | 删除关键词文档绑定 |
| [**queryDeleteNodePositions()**](QueriesApi.md#queryDeleteNodePositions) | **DELETE** /queries/{app}/node-positions | 清除查询条件的节点位置信息 |
| [**queryDeleteVariable()**](QueriesApi.md#queryDeleteVariable) | **DELETE** /queries/{app}/variables | 删除预定义查询变量 |
| [**queryFields()**](QueriesApi.md#queryFields) | **GET** /queries/{app}/fields | 获取查询关联的所有索引的字段信息 |
| [**queryGet()**](QueriesApi.md#queryGet) | **GET** /queries/{app}/{query} | 获取查询的详情 |
| [**queryGetNodePositions()**](QueriesApi.md#queryGetNodePositions) | **GET** /queries/{app}/node-positions | 获取查询条件的节点位置信息 |
| [**queryGetRecord()**](QueriesApi.md#queryGetRecord) | **GET** /queries/{app}/record | 获取记录的详情 |
| [**queryKeywordBindings()**](QueriesApi.md#queryKeywordBindings) | **GET** /queries/{app}/keyword-bindings | 获取查询的关键词文档绑定列表 |
| [**queryList()**](QueriesApi.md#queryList) | **GET** /queries/{app} | 获取应用下所有索引下的查询列表（按索引进行分组） |
| [**queryProfile()**](QueriesApi.md#queryProfile) | **GET** /queries/{app}/profiler | 获取搜索诊断信息 |
| [**queryRecordsOfKeywordBinding()**](QueriesApi.md#queryRecordsOfKeywordBinding) | **GET** /queries/{app}/keyword-bindings-records | 获取关键词绑定对应的记录列表 |
| [**querySaveIntelligentMappings()**](QueriesApi.md#querySaveIntelligentMappings) | **PUT** /queries/{app}/intelligent-mappings | 设置索引智能匹配字段 |
| [**querySaveNodePositions()**](QueriesApi.md#querySaveNodePositions) | **PUT** /queries/{app}/node-positions | 保存查询条件的节点位置信息 |
| [**querySearch()**](QueriesApi.md#querySearch) | **GET** /queries/{app}/search | 搜索测试 |
| [**querySource()**](QueriesApi.md#querySource) | **POST** /queries/{app}/{query} | 获取最终查询的源码(JSON) |
| [**querySuggest()**](QueriesApi.md#querySuggest) | **GET** /queries/{app}/suggest | 获取搜索建议列表 |
| [**queryTestIntelligentMappings()**](QueriesApi.md#queryTestIntelligentMappings) | **POST** /queries/{app}/intelligent-mappings | 测试索引智能匹配字段 |
| [**queryUpdate()**](QueriesApi.md#queryUpdate) | **PUT** /queries/{app} | 修改查询 |
| [**queryUpdateKeywordBindings()**](QueriesApi.md#queryUpdateKeywordBindings) | **PATCH** /queries/{app}/keyword-bindings | 修改关键词文档绑定 |
| [**queryUpdateSettings()**](QueriesApi.md#queryUpdateSettings) | **POST** /queries/{app}/settings | 更改查询的设置项 |
| [**queryUpdateVariable()**](QueriesApi.md#queryUpdateVariable) | **PATCH** /queries/{app}/variables | 修改预定义查询变量 |
| [**queryValidate()**](QueriesApi.md#queryValidate) | **GET** /queries/{app}/validate | 获取搜索验证结果 |
| [**queryValidateAggregation()**](QueriesApi.md#queryValidateAggregation) | **POST** /queries/{app}/validate-aggregation | 验证聚合定义是否正确 |
| [**queryValidateQuery()**](QueriesApi.md#queryValidateQuery) | **POST** /queries/{app}/validate-query | 验证聚合定义是否正确 |
| [**queryValidateScriptField()**](QueriesApi.md#queryValidateScriptField) | **POST** /queries/{app}/validate-script-field | 验证脚本字段是否正确 |
| [**queryValidateScriptScore()**](QueriesApi.md#queryValidateScriptScore) | **POST** /queries/{app}/validate-script-score | 验证脚本字段是否正确 |
| [**queryValidateSuggestion()**](QueriesApi.md#queryValidateSuggestion) | **POST** /queries/{app}/validate-suggest | 验证建议是否正确 |
| [**queryVariables()**](QueriesApi.md#queryVariables) | **GET** /queries/{app}/variables | 获取应用的预定义查询变量列表 |


## `queryAnalyze()`

```php
queryAnalyze($app, $analyze_object, $index): \OpenAPI\Client\modals\AnalyzeToken[]
```

分词测试

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$analyze_object = new \OpenAPI\Client\modals\AnalyzeObject(); // \OpenAPI\Client\modals\AnalyzeObject
$index = 0; // float | 索引编号，如果指定索引编号则使用索引的分词器

try {
    $result = $apiInstance->queryAnalyze($app, $analyze_object, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryAnalyze: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **analyze_object** | [**\OpenAPI\Client\modals\AnalyzeObject**](../Model/AnalyzeObject.md)|  | |
| **index** | **float**| 索引编号，如果指定索引编号则使用索引的分词器 | [optional] [default to 0] |

### Return type

[**\OpenAPI\Client\modals\AnalyzeToken[]**](../Model/AnalyzeToken.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryCopy()`

```php
queryCopy($app, $query): \OpenAPI\Client\modals\QueryBean
```

复制指定查询并创建新查询

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 源查询编号

try {
    $result = $apiInstance->queryCopy($app, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryCopy: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 源查询编号 | |

### Return type

[**\OpenAPI\Client\modals\QueryBean**](../Model/QueryBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryCopyToQuery()`

```php
queryCopyToQuery($app, $query, $to): bool
```

复制查询到已有查询

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 源查询编号
$to = 56; // int | 目标查询编号

try {
    $result = $apiInstance->queryCopyToQuery($app, $query, $to);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryCopyToQuery: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 源查询编号 | |
| **to** | **int**| 目标查询编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryCreate()`

```php
queryCreate($app, $query_form): \OpenAPI\Client\modals\QueryBean
```

创建搜索

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query_form = new \OpenAPI\Client\modals\QueryForm(); // \OpenAPI\Client\modals\QueryForm

try {
    $result = $apiInstance->queryCreate($app, $query_form);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryCreate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query_form** | [**\OpenAPI\Client\modals\QueryForm**](../Model/QueryForm.md)|  | |

### Return type

[**\OpenAPI\Client\modals\QueryBean**](../Model/QueryBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryCreateKeywordBindings()`

```php
queryCreateKeywordBindings($app, $query, $keyword_binding_bean): \OpenAPI\Client\modals\KeywordBindingBean
```

创建新的关键词文档绑定

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$keyword_binding_bean = new \OpenAPI\Client\modals\KeywordBindingBean(); // \OpenAPI\Client\modals\KeywordBindingBean

try {
    $result = $apiInstance->queryCreateKeywordBindings($app, $query, $keyword_binding_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryCreateKeywordBindings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **keyword_binding_bean** | [**\OpenAPI\Client\modals\KeywordBindingBean**](../Model/KeywordBindingBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\KeywordBindingBean**](../Model/KeywordBindingBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryCreateVariable()`

```php
queryCreateVariable($app, $query_variable_bean): \OpenAPI\Client\modals\QueryVariableBean
```

创建新的预定义查询变量



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query_variable_bean = new \OpenAPI\Client\modals\QueryVariableBean(); // \OpenAPI\Client\modals\QueryVariableBean

try {
    $result = $apiInstance->queryCreateVariable($app, $query_variable_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryCreateVariable: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query_variable_bean** | [**\OpenAPI\Client\modals\QueryVariableBean**](../Model/QueryVariableBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\QueryVariableBean**](../Model/QueryVariableBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryDelete()`

```php
queryDelete($app, $query): bool
```

删除搜索

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号

try {
    $result = $apiInstance->queryDelete($app, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryDeleteKeywordBindings()`

```php
queryDeleteKeywordBindings($app, $query, $id): bool
```

删除关键词文档绑定

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$id = 56; // int | 关键词编号

try {
    $result = $apiInstance->queryDeleteKeywordBindings($app, $query, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryDeleteKeywordBindings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **id** | **int**| 关键词编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryDeleteNodePositions()`

```php
queryDeleteNodePositions($app, $query): bool
```

清除查询条件的节点位置信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号

try {
    $result = $apiInstance->queryDeleteNodePositions($app, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryDeleteNodePositions: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryDeleteVariable()`

```php
queryDeleteVariable($app, $id): bool
```

删除预定义查询变量



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$id = 56; // int | 自定义查询变量编号

try {
    $result = $apiInstance->queryDeleteVariable($app, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryDeleteVariable: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **id** | **int**| 自定义查询变量编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryFields()`

```php
queryFields($app, $query): \OpenAPI\Client\modals\IndexFieldBean[]
```

获取查询关联的所有索引的字段信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号

try {
    $result = $apiInstance->queryFields($app, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryFields: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |

### Return type

[**\OpenAPI\Client\modals\IndexFieldBean[]**](../Model/IndexFieldBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryGet()`

```php
queryGet($app, $query): \OpenAPI\Client\modals\QueryBean
```

获取查询的详情

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号

try {
    $result = $apiInstance->queryGet($app, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |

### Return type

[**\OpenAPI\Client\modals\QueryBean**](../Model/QueryBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryGetNodePositions()`

```php
queryGetNodePositions($app, $query): object
```

获取查询条件的节点位置信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号

try {
    $result = $apiInstance->queryGetNodePositions($app, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryGetNodePositions: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryGetRecord()`

```php
queryGetRecord($app, $query, $_id): object
```

获取记录的详情

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$_id = '_id_example'; // string | 记录 _id 值

try {
    $result = $apiInstance->queryGetRecord($app, $query, $_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryGetRecord: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **_id** | **string**| 记录 _id 值 | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryKeywordBindings()`

```php
queryKeywordBindings($app, $query): \OpenAPI\Client\modals\KeywordBindingBean[]
```

获取查询的关键词文档绑定列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号

try {
    $result = $apiInstance->queryKeywordBindings($app, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryKeywordBindings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |

### Return type

[**\OpenAPI\Client\modals\KeywordBindingBean[]**](../Model/KeywordBindingBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryList()`

```php
queryList($app, $index): \OpenAPI\Client\modals\QueryBean[]
```

获取应用下所有索引下的查询列表（按索引进行分组）

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号

try {
    $result = $apiInstance->queryList($app, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryList: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | [optional] |

### Return type

[**\OpenAPI\Client\modals\QueryBean[]**](../Model/QueryBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryProfile()`

```php
queryProfile($app, $query, $q): object
```

获取搜索诊断信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 记录编号
$q = 'q_example'; // string | 诊断关键字

try {
    $result = $apiInstance->queryProfile($app, $query, $q);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryProfile: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 记录编号 | |
| **q** | **string**| 诊断关键字 | [optional] |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryRecordsOfKeywordBinding()`

```php
queryRecordsOfKeywordBinding($app, $id): object[]
```

获取关键词绑定对应的记录列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$id = 56; // int | 关键词绑定编号

try {
    $result = $apiInstance->queryRecordsOfKeywordBinding($app, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryRecordsOfKeywordBinding: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **id** | **int**| 关键词绑定编号 | |

### Return type

**object[]**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `querySaveIntelligentMappings()`

```php
querySaveIntelligentMappings($app, $query, $fields): bool
```

设置索引智能匹配字段

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$fields = array('fields_example'); // string[] | 字段列表

try {
    $result = $apiInstance->querySaveIntelligentMappings($app, $query, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->querySaveIntelligentMappings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **fields** | [**string[]**](../Model/string.md)| 字段列表 | [optional] |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `querySaveNodePositions()`

```php
querySaveNodePositions($app, $query, $body): object
```

保存查询条件的节点位置信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$body = array('key' => new \stdClass); // object

try {
    $result = $apiInstance->querySaveNodePositions($app, $query, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->querySaveNodePositions: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **body** | **object**|  | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `querySearch()`

```php
querySearch($app, $query, $from, $size, $q, $params): object
```

搜索测试

该接口主要用于定制查询的测试，必须授权才能访问

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$from = 0; // int | 起始记录
$size = 10; // int | 每页记录数量
$q = 'q_example'; // string | 查询关键字
$params = array('key' => 'params_example'); // array<string,string> | 聚合参数

try {
    $result = $apiInstance->querySearch($app, $query, $from, $size, $q, $params);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->querySearch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **from** | **int**| 起始记录 | [default to 0] |
| **size** | **int**| 每页记录数量 | [default to 10] |
| **q** | **string**| 查询关键字 | [optional] |
| **params** | [**array<string,string>**](../Model/string.md)| 聚合参数 | [optional] |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `querySource()`

```php
querySource($app, $query, $q): object
```

获取最终查询的源码(JSON)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$q = 'q_example'; // string | 搜索关键字

try {
    $result = $apiInstance->querySource($app, $query, $q);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->querySource: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **q** | **string**| 搜索关键字 | [optional] |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `querySuggest()`

```php
querySuggest($app, $query, $q): object[]
```

获取搜索建议列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$q = 'q_example'; // string | 搜索关键字

try {
    $result = $apiInstance->querySuggest($app, $query, $q);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->querySuggest: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **q** | **string**| 搜索关键字 | |

### Return type

**object[]**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryTestIntelligentMappings()`

```php
queryTestIntelligentMappings($app, $query, $q, $fields): \OpenAPI\Client\modals\IntelligentMapping[]
```

测试索引智能匹配字段

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$q = 'q_example'; // string | 搜索内容
$fields = array('fields_example'); // string[] | 字段列表

try {
    $result = $apiInstance->queryTestIntelligentMappings($app, $query, $q, $fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryTestIntelligentMappings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **q** | **string**| 搜索内容 | |
| **fields** | [**string[]**](../Model/string.md)| 字段列表 | [optional] |

### Return type

[**\OpenAPI\Client\modals\IntelligentMapping[]**](../Model/IntelligentMapping.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryUpdate()`

```php
queryUpdate($app, $query, $query_form): \OpenAPI\Client\modals\QueryBean
```

修改查询

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$query_form = new \OpenAPI\Client\modals\QueryForm(); // \OpenAPI\Client\modals\QueryForm

try {
    $result = $apiInstance->queryUpdate($app, $query, $query_form);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryUpdate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **query_form** | [**\OpenAPI\Client\modals\QueryForm**](../Model/QueryForm.md)|  | |

### Return type

[**\OpenAPI\Client\modals\QueryBean**](../Model/QueryBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryUpdateKeywordBindings()`

```php
queryUpdateKeywordBindings($app, $query, $keyword_binding_bean): \OpenAPI\Client\modals\KeywordBindingBean
```

修改关键词文档绑定

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$keyword_binding_bean = new \OpenAPI\Client\modals\KeywordBindingBean(); // \OpenAPI\Client\modals\KeywordBindingBean

try {
    $result = $apiInstance->queryUpdateKeywordBindings($app, $query, $keyword_binding_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryUpdateKeywordBindings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **keyword_binding_bean** | [**\OpenAPI\Client\modals\KeywordBindingBean**](../Model/KeywordBindingBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\KeywordBindingBean**](../Model/KeywordBindingBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryUpdateSettings()`

```php
queryUpdateSettings($app, $query, $name, $value, $type): bool
```

更改查询的设置项

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 记录编号
$name = 'name_example'; // string | 设置项名称
$value = 'value_example'; // string | 设置值
$type = 'string'; // string | 设置项类型

try {
    $result = $apiInstance->queryUpdateSettings($app, $query, $name, $value, $type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryUpdateSettings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 记录编号 | |
| **name** | **string**| 设置项名称 | |
| **value** | **string**| 设置值 | |
| **type** | **string**| 设置项类型 | [default to &#39;string&#39;] |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryUpdateVariable()`

```php
queryUpdateVariable($app, $id, $query_variable_bean): \OpenAPI\Client\modals\QueryVariableBean
```

修改预定义查询变量



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$id = 56; // int | 自定义查询变量编号
$query_variable_bean = new \OpenAPI\Client\modals\QueryVariableBean(); // \OpenAPI\Client\modals\QueryVariableBean

try {
    $result = $apiInstance->queryUpdateVariable($app, $id, $query_variable_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryUpdateVariable: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **id** | **int**| 自定义查询变量编号 | |
| **query_variable_bean** | [**\OpenAPI\Client\modals\QueryVariableBean**](../Model/QueryVariableBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\QueryVariableBean**](../Model/QueryVariableBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryValidate()`

```php
queryValidate($app, $query): object
```

获取搜索验证结果

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号

try {
    $result = $apiInstance->queryValidate($app, $query);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryValidate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryValidateAggregation()`

```php
queryValidateAggregation($app, $query, $body): object
```

验证聚合定义是否正确

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$body = array('key' => new \stdClass); // object

try {
    $result = $apiInstance->queryValidateAggregation($app, $query, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryValidateAggregation: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **body** | **object**|  | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryValidateQuery()`

```php
queryValidateQuery($app, $query, $body): object
```

验证聚合定义是否正确

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$body = array('key' => new \stdClass); // object

try {
    $result = $apiInstance->queryValidateQuery($app, $query, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryValidateQuery: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **body** | **object**|  | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryValidateScriptField()`

```php
queryValidateScriptField($app, $body, $query, $index): object
```

验证脚本字段是否正确

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$body = array('key' => new \stdClass); // object
$query = 56; // int | 查询编号, query 和 index 两个参数传一个即可
$index = 56; // int | 索引编号, query 和 index 两个参数传一个即可

try {
    $result = $apiInstance->queryValidateScriptField($app, $body, $query, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryValidateScriptField: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **body** | **object**|  | |
| **query** | **int**| 查询编号, query 和 index 两个参数传一个即可 | [optional] |
| **index** | **int**| 索引编号, query 和 index 两个参数传一个即可 | [optional] |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryValidateScriptScore()`

```php
queryValidateScriptScore($app, $query, $body): object
```

验证脚本字段是否正确

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$body = array('key' => new \stdClass); // object

try {
    $result = $apiInstance->queryValidateScriptScore($app, $query, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryValidateScriptScore: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **body** | **object**|  | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryValidateSuggestion()`

```php
queryValidateSuggestion($app, $query, $body): object
```

验证建议是否正确

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$query = 56; // int | 查询编号
$body = array('key' => new \stdClass); // object

try {
    $result = $apiInstance->queryValidateSuggestion($app, $query, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryValidateSuggestion: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **query** | **int**| 查询编号 | |
| **body** | **object**|  | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryVariables()`

```php
queryVariables($app): \OpenAPI\Client\modals\QueryVariableBean[]
```

获取应用的预定义查询变量列表



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\QueriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->queryVariables($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueriesApi->queryVariables: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\QueryVariableBean[]**](../Model/QueryVariableBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
