# OpenAPI\Client\FieldsApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**indexFields()**](FieldsApi.md#indexFields) | **GET** /indices/{app}/{index}/fields | 获取索引字段映射详情 |
| [**indexUpdateFields()**](FieldsApi.md#indexUpdateFields) | **POST** /indices/{app}/{index}/fields | 更新索引的字段映射 |
| [**indexUpdateHtmlStripFields()**](FieldsApi.md#indexUpdateHtmlStripFields) | **PATCH** /indices/{app}/{index}/fields | 更新索引的HTML过滤字段列表 |
| [**indexValuesOfField()**](FieldsApi.md#indexValuesOfField) | **GET** /indices/{app}/{index}/fields/{field} | 获取索引字段的所有值列表 |


## `indexFields()`

```php
indexFields($app, $index): object
```

获取索引字段映射详情

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\FieldsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号

try {
    $result = $apiInstance->indexFields($app, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FieldsApi->indexFields: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexUpdateFields()`

```php
indexUpdateFields($app, $index, $body): object
```

更新索引的字段映射

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\FieldsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$body = array('key' => new \stdClass); // object

try {
    $result = $apiInstance->indexUpdateFields($app, $index, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FieldsApi->indexUpdateFields: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **body** | **object**|  | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexUpdateHtmlStripFields()`

```php
indexUpdateHtmlStripFields($app, $index, $body): object
```

更新索引的HTML过滤字段列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\FieldsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$body = array('key' => new \stdClass); // object

try {
    $result = $apiInstance->indexUpdateHtmlStripFields($app, $index, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FieldsApi->indexUpdateHtmlStripFields: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **body** | **object**|  | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `indexValuesOfField()`

```php
indexValuesOfField($app, $index, $field, $size): \OpenAPI\Client\modals\ValueOfField[]
```

获取索引字段的所有值列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\FieldsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$field = 'field_example'; // string | 字段名称
$size = 20; // int | values count

try {
    $result = $apiInstance->indexValuesOfField($app, $index, $field, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FieldsApi->indexValuesOfField: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **field** | **string**| 字段名称 | |
| **size** | **int**| values count | [default to 20] |

### Return type

[**\OpenAPI\Client\modals\ValueOfField[]**](../Model/ValueOfField.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
