# OpenAPI\Client\WidgetsApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**widgetCopy()**](WidgetsApi.md#widgetCopy) | **POST** /widget/{app}/copy | 复制指定组件并创建新组件 |
| [**widgetCopyToWidget()**](WidgetsApi.md#widgetCopyToWidget) | **PUT** /widget/{app}/copy | 复制组件到已有组件 |
| [**widgetCreate()**](WidgetsApi.md#widgetCreate) | **POST** /widgets/{app} | 创建组件 |
| [**widgetDelete()**](WidgetsApi.md#widgetDelete) | **DELETE** /widgets/{app}/{widget} | 删除组件 |
| [**widgetDeleteLogo()**](WidgetsApi.md#widgetDeleteLogo) | **DELETE** /widgets/{app}/{widget}/logo | 删除组件 Logo |
| [**widgetDetail()**](WidgetsApi.md#widgetDetail) | **GET** /widget/{ident} | 获取UI组件的所有相关信息 |
| [**widgetDownload()**](WidgetsApi.md#widgetDownload) | **GET** /widgets/{app}/{widget}/download | 下载组件应用源码 |
| [**widgetGet()**](WidgetsApi.md#widgetGet) | **GET** /widgets/{app}/{widget} | 获取组件的详情 |
| [**widgetList()**](WidgetsApi.md#widgetList) | **GET** /widgets/{app} | 获取应用的组件列表 |
| [**widgetLogo()**](WidgetsApi.md#widgetLogo) | **POST** /widgets/{app}/{widget}/logo | 设置组件 Logo |
| [**widgetUpdate()**](WidgetsApi.md#widgetUpdate) | **PUT** /widgets/{app}/{widget} | 修改组件 |
| [**widgetUpdateSettings()**](WidgetsApi.md#widgetUpdateSettings) | **PATCH** /widgets/{app}/{widget} | 修改组件设置参数 |


## `widgetCopy()`

```php
widgetCopy($app, $widget): \OpenAPI\Client\modals\WidgetBean
```

复制指定组件并创建新组件

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$widget = 56; // int | 源组件编号

try {
    $result = $apiInstance->widgetCopy($app, $widget);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetCopy: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **widget** | **int**| 源组件编号 | |

### Return type

[**\OpenAPI\Client\modals\WidgetBean**](../Model/WidgetBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `widgetCopyToWidget()`

```php
widgetCopyToWidget($app, $widget, $to): bool
```

复制组件到已有组件

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$widget = 56; // int | 源组件编号
$to = 56; // int | 目标组件编号

try {
    $result = $apiInstance->widgetCopyToWidget($app, $widget, $to);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetCopyToWidget: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **widget** | **int**| 源组件编号 | |
| **to** | **int**| 目标组件编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `widgetCreate()`

```php
widgetCreate($app, $widget_form): \OpenAPI\Client\modals\WidgetBean
```

创建组件

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$widget_form = new \OpenAPI\Client\modals\WidgetForm(); // \OpenAPI\Client\modals\WidgetForm

try {
    $result = $apiInstance->widgetCreate($app, $widget_form);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetCreate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **widget_form** | [**\OpenAPI\Client\modals\WidgetForm**](../Model/WidgetForm.md)|  | |

### Return type

[**\OpenAPI\Client\modals\WidgetBean**](../Model/WidgetBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `widgetDelete()`

```php
widgetDelete($app, $widget, $password): bool
```

删除组件

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$widget = 56; // int | 组件编号
$password = 'password_example'; // string

try {
    $result = $apiInstance->widgetDelete($app, $widget, $password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **widget** | **int**| 组件编号 | |
| **password** | **string**|  | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `widgetDeleteLogo()`

```php
widgetDeleteLogo($app, $widget): bool
```

删除组件 Logo

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$widget = 56; // int | 组件编号

try {
    $result = $apiInstance->widgetDeleteLogo($app, $widget);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetDeleteLogo: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **widget** | **int**| 组件编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `widgetDetail()`

```php
widgetDetail($ident, $x_token): \OpenAPI\Client\modals\WidgetBean
```

获取UI组件的所有相关信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ident = 'ident_example'; // string | UI组件的唯一标识
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权

try {
    $result = $apiInstance->widgetDetail($ident, $x_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetDetail: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **ident** | **string**| UI组件的唯一标识 | |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |

### Return type

[**\OpenAPI\Client\modals\WidgetBean**](../Model/WidgetBean.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `widgetDownload()`

```php
widgetDownload($app, $widget, $framework): \SplFileObject
```

下载组件应用源码

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$widget = 56; // int | 组件编号
$framework = 'framework_example'; // string | 指定的技术框架

try {
    $result = $apiInstance->widgetDownload($app, $widget, $framework);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetDownload: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **widget** | **int**| 组件编号 | |
| **framework** | **string**| 指定的技术框架 | |

### Return type

**\SplFileObject**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `default`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `widgetGet()`

```php
widgetGet($app, $widget): \OpenAPI\Client\modals\WidgetBean
```

获取组件的详情

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$widget = 56; // int | 组件编号

try {
    $result = $apiInstance->widgetGet($app, $widget);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **widget** | **int**| 组件编号 | |

### Return type

[**\OpenAPI\Client\modals\WidgetBean**](../Model/WidgetBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `widgetList()`

```php
widgetList($app): \OpenAPI\Client\modals\WidgetBean[]
```

获取应用的组件列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->widgetList($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetList: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\WidgetBean[]**](../Model/WidgetBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `widgetLogo()`

```php
widgetLogo($app, $widget, $logo): \OpenAPI\Client\modals\WidgetLogo
```

设置组件 Logo



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$widget = 56; // int | 组件编号
$logo = "/path/to/file.txt"; // \SplFileObject

try {
    $result = $apiInstance->widgetLogo($app, $widget, $logo);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetLogo: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **widget** | **int**| 组件编号 | |
| **logo** | **\SplFileObject****\SplFileObject**|  | [optional] |

### Return type

[**\OpenAPI\Client\modals\WidgetLogo**](../Model/WidgetLogo.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `widgetUpdate()`

```php
widgetUpdate($app, $widget, $widget_form): \OpenAPI\Client\modals\WidgetBean
```

修改组件

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$widget = 56; // int | 组件编号
$widget_form = new \OpenAPI\Client\modals\WidgetForm(); // \OpenAPI\Client\modals\WidgetForm

try {
    $result = $apiInstance->widgetUpdate($app, $widget, $widget_form);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetUpdate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **widget** | **int**| 组件编号 | |
| **widget_form** | [**\OpenAPI\Client\modals\WidgetForm**](../Model/WidgetForm.md)|  | |

### Return type

[**\OpenAPI\Client\modals\WidgetBean**](../Model/WidgetBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `widgetUpdateSettings()`

```php
widgetUpdateSettings($app, $widget, $key, $type, $value, $vcode): bool
```

修改组件设置参数

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\WidgetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$widget = 56; // int | 组件编号
$key = 'key_example'; // string | 配置项名称
$type = 'type_example'; // string | 配置值类型
$value = 'value_example'; // string | 配置值
$vcode = 'vcode_example'; // string | 验证码(非必须，只有在配置通知邮箱和手机的时候才需要)

try {
    $result = $apiInstance->widgetUpdateSettings($app, $widget, $key, $type, $value, $vcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WidgetsApi->widgetUpdateSettings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **widget** | **int**| 组件编号 | |
| **key** | **string**| 配置项名称 | |
| **type** | **string**| 配置值类型 | |
| **value** | **string**| 配置值 | |
| **vcode** | **string**| 验证码(非必须，只有在配置通知邮箱和手机的时候才需要) | [optional] |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
