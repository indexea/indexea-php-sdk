# OpenAPI\Client\AccountApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**accountBulletin()**](AccountApi.md#accountBulletin) | **GET** /accounts/bulletin | 获取系统公告 |
| [**accountDelete()**](AccountApi.md#accountDelete) | **DELETE** /account/profile | 注销账号 |
| [**accountDeleteOpenid()**](AccountApi.md#accountDeleteOpenid) | **DELETE** /account/openid | 解绑三方账号 |
| [**accountOpenid()**](AccountApi.md#accountOpenid) | **GET** /account/openid | 获取绑定的所有三方账号 |
| [**accountPasswd()**](AccountApi.md#accountPasswd) | **POST** /account/passwd | 修改账号密码 |
| [**accountPortrait()**](AccountApi.md#accountPortrait) | **POST** /account/portrait | 修改账号头像 |
| [**accountProfile()**](AccountApi.md#accountProfile) | **GET** /account/profile | 获取登录账号信息 |
| [**accountResetPwd()**](AccountApi.md#accountResetPwd) | **POST** /account/reset-pwd | 重置账号密码 |
| [**accountSendVerifyCode()**](AccountApi.md#accountSendVerifyCode) | **GET** /account/send-verify-code | 发送账号验证码 |
| [**accountSignin()**](AccountApi.md#accountSignin) | **POST** /account/signin | 登录系统 |
| [**accountSignout()**](AccountApi.md#accountSignout) | **POST** /account/sign-out | 退出登录状态 |
| [**accountSignup()**](AccountApi.md#accountSignup) | **POST** /account/signup | 注册新账号 |
| [**accountUpdate()**](AccountApi.md#accountUpdate) | **POST** /account/profile | 修改账号资料 |
| [**accountUpdateSettings()**](AccountApi.md#accountUpdateSettings) | **POST** /account/settings | 修改账号设置 |


## `accountBulletin()`

```php
accountBulletin(): \OpenAPI\Client\modals\Bulletin
```

获取系统公告

获取系统公告

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->accountBulletin();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountBulletin: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenAPI\Client\modals\Bulletin**](../Model/Bulletin.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountDelete()`

```php
accountDelete($pwd, $vcode): bool
```

注销账号



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$pwd = 'pwd_example'; // string | 操作者的登录密码
$vcode = 'vcode_example'; // string | 操作验证码

try {
    $result = $apiInstance->accountDelete($pwd, $vcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **pwd** | **string**| 操作者的登录密码 | |
| **vcode** | **string**| 操作验证码 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountDeleteOpenid()`

```php
accountDeleteOpenid($type, $openid): bool
```

解绑三方账号



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$type = 'type_example'; // string | 三方账号类型
$openid = 'openid_example'; // string | 三方账号唯一标识

try {
    $result = $apiInstance->accountDeleteOpenid($type, $openid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountDeleteOpenid: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **type** | **string**| 三方账号类型 | |
| **openid** | **string**| 三方账号唯一标识 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountOpenid()`

```php
accountOpenid(): \OpenAPI\Client\modals\OpenidBean[]
```

获取绑定的所有三方账号



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->accountOpenid();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountOpenid: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\OpenAPI\Client\modals\OpenidBean[]**](../Model/OpenidBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountPasswd()`

```php
accountPasswd($password, $new_password): bool
```

修改账号密码



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$password = 'password_example'; // string | 旧密码
$new_password = 'new_password_example'; // string | 新密码

try {
    $result = $apiInstance->accountPasswd($password, $new_password);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountPasswd: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **password** | **string**| 旧密码 | |
| **new_password** | **string**| 新密码 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountPortrait()`

```php
accountPortrait($portrait): \OpenAPI\Client\modals\AccountBean
```

修改账号头像



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$portrait = "/path/to/file.txt"; // \SplFileObject

try {
    $result = $apiInstance->accountPortrait($portrait);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountPortrait: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **portrait** | **\SplFileObject****\SplFileObject**|  | [optional] |

### Return type

[**\OpenAPI\Client\modals\AccountBean**](../Model/AccountBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountProfile()`

```php
accountProfile($account): \OpenAPI\Client\modals\AccountBean
```

获取登录账号信息



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account = 'account_example'; // string | 获取指定账号信息，如果不指定，则返回当前登录账号

try {
    $result = $apiInstance->accountProfile($account);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountProfile: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **account** | **string**| 获取指定账号信息，如果不指定，则返回当前登录账号 | [optional] |

### Return type

[**\OpenAPI\Client\modals\AccountBean**](../Model/AccountBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountResetPwd()`

```php
accountResetPwd($account, $verify_code, $pwd): bool
```

重置账号密码



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$account = 'account_example'; // string
$verify_code = 'verify_code_example'; // string
$pwd = 'pwd_example'; // string

try {
    $result = $apiInstance->accountResetPwd($account, $verify_code, $pwd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountResetPwd: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **account** | **string**|  | |
| **verify_code** | **string**|  | |
| **pwd** | **string**|  | |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountSendVerifyCode()`

```php
accountSendVerifyCode($account, $purpose): bool
```

发送账号验证码



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$account = 'account_example'; // string | 账号
$purpose = 'purpose_example'; // string | 验证码的用途

try {
    $result = $apiInstance->accountSendVerifyCode($account, $purpose);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountSendVerifyCode: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **account** | **string**| 账号 | |
| **purpose** | **string**| 验证码的用途 | |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountSignin()`

```php
accountSignin($account, $pwd, $keep_login): \OpenAPI\Client\modals\AccountBean
```

登录系统



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$account = 'account_example'; // string
$pwd = 'pwd_example'; // string
$keep_login = false; // bool

try {
    $result = $apiInstance->accountSignin($account, $pwd, $keep_login);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountSignin: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **account** | **string**|  | |
| **pwd** | **string**|  | |
| **keep_login** | **bool**|  | [optional] [default to false] |

### Return type

[**\OpenAPI\Client\modals\AccountBean**](../Model/AccountBean.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountSignout()`

```php
accountSignout()
```

退出登录状态



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $apiInstance->accountSignout();
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountSignout: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountSignup()`

```php
accountSignup($account, $pwd, $name, $activate_code): \OpenAPI\Client\modals\AccountBean
```

注册新账号



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$account = 'account_example'; // string
$pwd = 'pwd_example'; // string
$name = 'name_example'; // string
$activate_code = 'activate_code_example'; // string

try {
    $result = $apiInstance->accountSignup($account, $pwd, $name, $activate_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountSignup: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **account** | **string**|  | |
| **pwd** | **string**|  | |
| **name** | **string**|  | |
| **activate_code** | **string**|  | |

### Return type

[**\OpenAPI\Client\modals\AccountBean**](../Model/AccountBean.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountUpdate()`

```php
accountUpdate($account_bean): \OpenAPI\Client\modals\AccountBean
```

修改账号资料



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$account_bean = new \OpenAPI\Client\modals\AccountBean(); // \OpenAPI\Client\modals\AccountBean

try {
    $result = $apiInstance->accountUpdate($account_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountUpdate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **account_bean** | [**\OpenAPI\Client\modals\AccountBean**](../Model/AccountBean.md)|  | |

### Return type

[**\OpenAPI\Client\modals\AccountBean**](../Model/AccountBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountUpdateSettings()`

```php
accountUpdateSettings($key, $type, $value, $vcode): bool
```

修改账号设置



### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$key = 'key_example'; // string | 配置项名称
$type = 'type_example'; // string | 配置值类型
$value = 'value_example'; // string | 配置值
$vcode = 'vcode_example'; // string | 验证码(非必须，只有在配置通知邮箱和手机的时候才需要)

try {
    $result = $apiInstance->accountUpdateSettings($key, $type, $value, $vcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountUpdateSettings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **key** | **string**| 配置项名称 | |
| **type** | **string**| 配置值类型 | |
| **value** | **string**| 配置值 | |
| **vcode** | **string**| 验证码(非必须，只有在配置通知邮箱和手机的时候才需要) | [optional] |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/x-www-form-urlencoded`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
