# OpenAPI\Client\RecommendApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**recommendClick()**](RecommendApi.md#recommendClick) | **POST** /recommend/{ident}/click | 推荐结果点击行为收集 |
| [**recommendCreate()**](RecommendApi.md#recommendCreate) | **POST** /recommends/{app} | 创建新的推荐 |
| [**recommendDelete()**](RecommendApi.md#recommendDelete) | **DELETE** /recommends/{app} | 删除推荐 |
| [**recommendDetail()**](RecommendApi.md#recommendDetail) | **GET** /recommend/{ident} | 获取推荐的记录列表 |
| [**recommendFetch()**](RecommendApi.md#recommendFetch) | **POST** /recommend/{ident} | 获取推荐的记录列表 |
| [**recommendList()**](RecommendApi.md#recommendList) | **GET** /recommends/{app} | 获取已定义的推荐列表 |
| [**recommendUpdate()**](RecommendApi.md#recommendUpdate) | **PUT** /recommends/{app} | 更新推荐信息 |


## `recommendClick()`

```php
recommendClick($ident, $action_id, $doc_id, $userid, $x_token): bool
```

推荐结果点击行为收集

该接口主要用于记录用户对推荐结果的点击行为

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\RecommendApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ident = 'ident_example'; // string | 推荐的唯一标识
$action_id = 'action_id_example'; // string | 对应推荐行为编号
$doc_id = 'doc_id_example'; // string | 对应索引中的内部记录编号
$userid = 'userid_example'; // string | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权

try {
    $result = $apiInstance->recommendClick($ident, $action_id, $doc_id, $userid, $x_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecommendApi->recommendClick: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **ident** | **string**| 推荐的唯一标识 | |
| **action_id** | **string**| 对应推荐行为编号 | |
| **doc_id** | **string**| 对应索引中的内部记录编号 | |
| **userid** | **string**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |

### Return type

**bool**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recommendCreate()`

```php
recommendCreate($app, $recommend_bean): \OpenAPI\Client\modals\RecommendBean
```

创建新的推荐

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecommendApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$recommend_bean = new \OpenAPI\Client\modals\RecommendBean(); // \OpenAPI\Client\modals\RecommendBean | 推荐信息

try {
    $result = $apiInstance->recommendCreate($app, $recommend_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecommendApi->recommendCreate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **recommend_bean** | [**\OpenAPI\Client\modals\RecommendBean**](../Model/RecommendBean.md)| 推荐信息 | |

### Return type

[**\OpenAPI\Client\modals\RecommendBean**](../Model/RecommendBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recommendDelete()`

```php
recommendDelete($app, $id): bool
```

删除推荐

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecommendApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$id = 56; // int | 推荐编号

try {
    $result = $apiInstance->recommendDelete($app, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecommendApi->recommendDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **id** | **int**| 推荐编号 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recommendDetail()`

```php
recommendDetail($ident, $x_token): \OpenAPI\Client\modals\RecommendBean
```

获取推荐的记录列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\RecommendApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ident = 'ident_example'; // string | 推荐定义的标识
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权

try {
    $result = $apiInstance->recommendDetail($ident, $x_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecommendApi->recommendDetail: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **ident** | **string**| 推荐定义的标识 | |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |

### Return type

[**\OpenAPI\Client\modals\RecommendBean**](../Model/RecommendBean.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recommendFetch()`

```php
recommendFetch($ident, $x_token, $userid, $condition, $from, $count): object
```

获取推荐的记录列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new OpenAPI\Client\Api\RecommendApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$ident = 'ident_example'; // string | 推荐定义的标识
$x_token = 'x_token_example'; // string | 如果要使用非发布的组件，需要组件作者授权
$userid = 'userid_example'; // string | 访客的唯一标识，该标识由搜索前端生成，长度不超过64
$condition = array('key' => 'condition_example'); // array<string,string> | 获取某个记录的参数,例如 id=11223(后端将使用 term query 进行匹配)
$from = 0; // int | 起始值
$count = 10; // int | 推荐的记录数

try {
    $result = $apiInstance->recommendFetch($ident, $x_token, $userid, $condition, $from, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecommendApi->recommendFetch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **ident** | **string**| 推荐定义的标识 | |
| **x_token** | **string**| 如果要使用非发布的组件，需要组件作者授权 | [optional] |
| **userid** | **string**| 访客的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] |
| **condition** | [**array<string,string>**](../Model/string.md)| 获取某个记录的参数,例如 id&#x3D;11223(后端将使用 term query 进行匹配) | [optional] |
| **from** | **int**| 起始值 | [optional] [default to 0] |
| **count** | **int**| 推荐的记录数 | [optional] [default to 10] |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recommendList()`

```php
recommendList($app): \OpenAPI\Client\modals\RecommendBean[]
```

获取已定义的推荐列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecommendApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识

try {
    $result = $apiInstance->recommendList($app);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecommendApi->recommendList: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |

### Return type

[**\OpenAPI\Client\modals\RecommendBean[]**](../Model/RecommendBean.md)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recommendUpdate()`

```php
recommendUpdate($app, $recommend_bean): bool
```

更新推荐信息

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecommendApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$recommend_bean = new \OpenAPI\Client\modals\RecommendBean(); // \OpenAPI\Client\modals\RecommendBean | 推荐信息

try {
    $result = $apiInstance->recommendUpdate($app, $recommend_bean);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecommendApi->recommendUpdate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **recommend_bean** | [**\OpenAPI\Client\modals\RecommendBean**](../Model/RecommendBean.md)| 推荐信息 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
