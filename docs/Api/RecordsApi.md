# OpenAPI\Client\RecordsApi

All URIs are relative to https://api.indexea.com/v1, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**recordDelete()**](RecordsApi.md#recordDelete) | **DELETE** /records/{app}/{index} | 删除记录数据 |
| [**recordDeleteByQuery()**](RecordsApi.md#recordDeleteByQuery) | **DELETE** /records/{app}/{index}/bulk | 批量删除记录数据(delete_by_query)，该接口传递查询参数获取要删除的记录并逐一删除 |
| [**recordGet()**](RecordsApi.md#recordGet) | **GET** /records/{app}/{index} | 获取单条记录详情 |
| [**recordList()**](RecordsApi.md#recordList) | **POST** /records/{app}/{index} | 获取索引记录列表 |
| [**recordPush()**](RecordsApi.md#recordPush) | **PUT** /records/{app}/{index} | 插入或者更新索引数据 |
| [**recordUpdateByQuery()**](RecordsApi.md#recordUpdateByQuery) | **PATCH** /records/{app}/{index}/bulk | 批量修改记录数据(update_by_query)，该接口传递查询参数获取要更新的记录，并使用 body 中的对象进行记录合并更新 |
| [**recordUpload()**](RecordsApi.md#recordUpload) | **POST** /records/{app}/{index}/bulk | 上传记录 |
| [**recordUploadOld()**](RecordsApi.md#recordUploadOld) | **POST** /records/{app}/{index}/upload | 上传记录，该接口已经废弃，请使用 /records/{app}/{index}/bulk 接口 |


## `recordDelete()`

```php
recordDelete($app, $index, $_id): bool
```

删除记录数据

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$_id = array('_id_example'); // string[] | 主键字段值

try {
    $result = $apiInstance->recordDelete($app, $index, $_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecordsApi->recordDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **_id** | [**string[]**](../Model/string.md)| 主键字段值 | |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recordDeleteByQuery()`

```php
recordDeleteByQuery($app, $index, $query, $params)
```

批量删除记录数据(delete_by_query)，该接口传递查询参数获取要删除的记录并逐一删除

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$query = 56; // int | 查询编号
$params = array('key' => 'params_example'); // array<string,string> | 查询参数

try {
    $apiInstance->recordDeleteByQuery($app, $index, $query, $params);
} catch (Exception $e) {
    echo 'Exception when calling RecordsApi->recordDeleteByQuery: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **query** | **int**| 查询编号 | |
| **params** | [**array<string,string>**](../Model/string.md)| 查询参数 | [optional] |

### Return type

void (empty response body)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recordGet()`

```php
recordGet($app, $index, $_id): object
```

获取单条记录详情

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$_id = '_id_example'; // string | 记录 _id 值

try {
    $result = $apiInstance->recordGet($app, $index, $_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecordsApi->recordGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **_id** | **string**| 记录 _id 值 | |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recordList()`

```php
recordList($app, $index, $q, $field, $from, $size, $save_filter, $record_filter): object[]
```

获取索引记录列表

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$q = 'q_example'; // string | 搜索关键字
$field = 'field_example'; // string | 搜索字段
$from = 0; // int | 起始记录
$size = 20; // int | 获取记录数
$save_filter = false; // bool | 是否保存过滤器信息
$record_filter = new \OpenAPI\Client\modals\RecordFilter(); // \OpenAPI\Client\modals\RecordFilter

try {
    $result = $apiInstance->recordList($app, $index, $q, $field, $from, $size, $save_filter, $record_filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecordsApi->recordList: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **q** | **string**| 搜索关键字 | [optional] |
| **field** | **string**| 搜索字段 | [optional] |
| **from** | **int**| 起始记录 | [optional] [default to 0] |
| **size** | **int**| 获取记录数 | [optional] [default to 20] |
| **save_filter** | **bool**| 是否保存过滤器信息 | [optional] [default to false] |
| **record_filter** | [**\OpenAPI\Client\modals\RecordFilter**](../Model/RecordFilter.md)|  | [optional] |

### Return type

**object[]**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recordPush()`

```php
recordPush($app, $index, $request_body, $combine): bool
```

插入或者更新索引数据

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$request_body = array(new \stdClass); // object[]
$combine = false; // bool | 更新策略：合并还是替换，combine=true 为合并模式

try {
    $result = $apiInstance->recordPush($app, $index, $request_body, $combine);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecordsApi->recordPush: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **request_body** | [**object[]**](../Model/object.md)|  | |
| **combine** | **bool**| 更新策略：合并还是替换，combine&#x3D;true 为合并模式 | [optional] [default to false] |

### Return type

**bool**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recordUpdateByQuery()`

```php
recordUpdateByQuery($app, $index, $query, $body, $params)
```

批量修改记录数据(update_by_query)，该接口传递查询参数获取要更新的记录，并使用 body 中的对象进行记录合并更新

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$query = 56; // int | 查询编号
$body = array('key' => new \stdClass); // object | 要更新的字段和对应的新字段值，只支持最基本类型
$params = array('key' => 'params_example'); // array<string,string> | 查询参数

try {
    $apiInstance->recordUpdateByQuery($app, $index, $query, $body, $params);
} catch (Exception $e) {
    echo 'Exception when calling RecordsApi->recordUpdateByQuery: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **query** | **int**| 查询编号 | |
| **body** | **object**| 要更新的字段和对应的新字段值，只支持最基本类型 | |
| **params** | [**array<string,string>**](../Model/string.md)| 查询参数 | [optional] |

### Return type

void (empty response body)

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recordUpload()`

```php
recordUpload($app, $index, $combine, $use_id_as_id_value, $files): object
```

上传记录

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$combine = false; // bool | 更新策略：合并还是替换，combine=true 为合并模式
$use_id_as_id_value = true; // bool | 使用数据中的 id 值作为记录 _id, 如果没有 id 字段则自动生成
$files = array("/path/to/file.txt"); // \SplFileObject[]

try {
    $result = $apiInstance->recordUpload($app, $index, $combine, $use_id_as_id_value, $files);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecordsApi->recordUpload: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **combine** | **bool**| 更新策略：合并还是替换，combine&#x3D;true 为合并模式 | [optional] [default to false] |
| **use_id_as_id_value** | **bool**| 使用数据中的 id 值作为记录 _id, 如果没有 id 字段则自动生成 | [optional] [default to true] |
| **files** | **\SplFileObject[]**|  | [optional] |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `recordUploadOld()`

```php
recordUploadOld($app, $index, $combine, $use_id_as_id_value, $files): object
```

上传记录，该接口已经废弃，请使用 /records/{app}/{index}/bulk 接口

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\RecordsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$app = 'app_example'; // string | 应用标识
$index = 56; // int | 索引编号
$combine = false; // bool | 更新策略：合并还是替换，combine=true 为合并模式
$use_id_as_id_value = true; // bool | 使用数据中的 id 值作为记录 _id, 如果没有 id 字段则自动生成
$files = array("/path/to/file.txt"); // \SplFileObject[]

try {
    $result = $apiInstance->recordUploadOld($app, $index, $combine, $use_id_as_id_value, $files);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecordsApi->recordUploadOld: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **app** | **string**| 应用标识 | |
| **index** | **int**| 索引编号 | |
| **combine** | **bool**| 更新策略：合并还是替换，combine&#x3D;true 为合并模式 | [optional] [default to false] |
| **use_id_as_id_value** | **bool**| 使用数据中的 id 值作为记录 _id, 如果没有 id 字段则自动生成 | [optional] [default to true] |
| **files** | **\SplFileObject[]**|  | [optional] |

### Return type

**object**

### Authorization

[TokenAuth](../../README.md#TokenAuth)

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
