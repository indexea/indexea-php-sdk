# indexea

这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。

### Errors

本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：

```
{
  \"error\": 404,
  \"message\": \"page not found\"
}
```


For more information, please visit [https://indexea.com/about](https://indexea.com/about).

## Installation & Usage

### Requirements

PHP 7.4 and later.
Should also work with PHP 8.0.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/indexea/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure Bearer (Random) authorization: TokenAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->accountBulletin();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountBulletin: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://api.indexea.com/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountApi* | [**accountBulletin**](docs/Api/AccountApi.md#accountbulletin) | **GET** /accounts/bulletin | 获取系统公告
*AccountApi* | [**accountDelete**](docs/Api/AccountApi.md#accountdelete) | **DELETE** /account/profile | 注销账号
*AccountApi* | [**accountDeleteOpenid**](docs/Api/AccountApi.md#accountdeleteopenid) | **DELETE** /account/openid | 解绑三方账号
*AccountApi* | [**accountOpenid**](docs/Api/AccountApi.md#accountopenid) | **GET** /account/openid | 获取绑定的所有三方账号
*AccountApi* | [**accountPasswd**](docs/Api/AccountApi.md#accountpasswd) | **POST** /account/passwd | 修改账号密码
*AccountApi* | [**accountPortrait**](docs/Api/AccountApi.md#accountportrait) | **POST** /account/portrait | 修改账号头像
*AccountApi* | [**accountProfile**](docs/Api/AccountApi.md#accountprofile) | **GET** /account/profile | 获取登录账号信息
*AccountApi* | [**accountResetPwd**](docs/Api/AccountApi.md#accountresetpwd) | **POST** /account/reset-pwd | 重置账号密码
*AccountApi* | [**accountSendVerifyCode**](docs/Api/AccountApi.md#accountsendverifycode) | **GET** /account/send-verify-code | 发送账号验证码
*AccountApi* | [**accountSignin**](docs/Api/AccountApi.md#accountsignin) | **POST** /account/signin | 登录系统
*AccountApi* | [**accountSignout**](docs/Api/AccountApi.md#accountsignout) | **POST** /account/sign-out | 退出登录状态
*AccountApi* | [**accountSignup**](docs/Api/AccountApi.md#accountsignup) | **POST** /account/signup | 注册新账号
*AccountApi* | [**accountUpdate**](docs/Api/AccountApi.md#accountupdate) | **POST** /account/profile | 修改账号资料
*AccountApi* | [**accountUpdateSettings**](docs/Api/AccountApi.md#accountupdatesettings) | **POST** /account/settings | 修改账号设置
*AppsApi* | [**appAddMentor**](docs/Api/AppsApi.md#appaddmentor) | **POST** /apps/{app}/mentors | 添加应用成员
*AppsApi* | [**appBlacklist**](docs/Api/AppsApi.md#appblacklist) | **GET** /apps/{app}/blacklist | 获取黑名单信息
*AppsApi* | [**appCreate**](docs/Api/AppsApi.md#appcreate) | **POST** /apps | 创建应用
*AppsApi* | [**appCreateAccessToken**](docs/Api/AppsApi.md#appcreateaccesstoken) | **POST** /apps/{app}/tokens | 创建 Access Token
*AppsApi* | [**appCreateOauthApp**](docs/Api/AppsApi.md#appcreateoauthapp) | **POST** /apps/{app}/oauth | 创建第三方应用
*AppsApi* | [**appDelete**](docs/Api/AppsApi.md#appdelete) | **DELETE** /apps/{app} | 删除应用
*AppsApi* | [**appDeleteAccessToken**](docs/Api/AppsApi.md#appdeleteaccesstoken) | **DELETE** /apps/{app}/tokens | 删除 Access Token
*AppsApi* | [**appDeleteMentor**](docs/Api/AppsApi.md#appdeletementor) | **DELETE** /apps/{app}/mentors | 删除应用成员
*AppsApi* | [**appDeleteOauthApp**](docs/Api/AppsApi.md#appdeleteoauthapp) | **DELETE** /apps/{app}/oauth | 删除第三方应用
*AppsApi* | [**appExcelOfLogs**](docs/Api/AppsApi.md#appexceloflogs) | **GET** /apps/{app}/logs | 导出应用日志到 Excel
*AppsApi* | [**appGet**](docs/Api/AppsApi.md#appget) | **GET** /apps/{app} | 获取应用详情
*AppsApi* | [**appGetCompany**](docs/Api/AppsApi.md#appgetcompany) | **POST** /apps/{app}/company | 获取应用填写的公司信息
*AppsApi* | [**appGetCompanyPic**](docs/Api/AppsApi.md#appgetcompanypic) | **GET** /apps/{app}/company | 获取公司营业执照或者一般纳税人证明
*AppsApi* | [**appList**](docs/Api/AppsApi.md#applist) | **GET** /apps | 获取应用列表
*AppsApi* | [**appListMentors**](docs/Api/AppsApi.md#applistmentors) | **GET** /apps/{app}/mentors | 获取应用成员列表
*AppsApi* | [**appListOauthApps**](docs/Api/AppsApi.md#applistoauthapps) | **GET** /apps/{app}/oauth | 获取第三方应用列表
*AppsApi* | [**appLogs**](docs/Api/AppsApi.md#applogs) | **POST** /apps/{app}/logs | 获取应用的日志列表
*AppsApi* | [**appResetAccessToken**](docs/Api/AppsApi.md#appresetaccesstoken) | **PATCH** /apps/{app}/tokens | 重置 Access Token
*AppsApi* | [**appResetOauthAppSecret**](docs/Api/AppsApi.md#appresetoauthappsecret) | **POST** /apps/{app}/oauth-reset-secret | 重新生成三方应用的密钥
*AppsApi* | [**appSaveBlacklist**](docs/Api/AppsApi.md#appsaveblacklist) | **PUT** /apps/{app}/blacklist | 修改应用的黑名单信息
*AppsApi* | [**appSaveCompany**](docs/Api/AppsApi.md#appsavecompany) | **PUT** /apps/{app}/company | 修改应用的公司信息
*AppsApi* | [**appSearchsEstimate**](docs/Api/AppsApi.md#appsearchsestimate) | **GET** /apps/{app}/searchs-estimate | 获取搜索流量包使用配额信息
*AppsApi* | [**appSetTrigger**](docs/Api/AppsApi.md#appsettrigger) | **PUT** /apps/{app}/trigger | 修改应用的触发器信息
*AppsApi* | [**appTokens**](docs/Api/AppsApi.md#apptokens) | **GET** /apps/{app}/tokens | 获取 Access Token 列表
*AppsApi* | [**appTransfer**](docs/Api/AppsApi.md#apptransfer) | **POST** /apps/{app}/transfer | 转让应用给他人
*AppsApi* | [**appTrigger**](docs/Api/AppsApi.md#apptrigger) | **GET** /apps/{app}/trigger | 获取应用触发器详情
*AppsApi* | [**appTriggerLogs**](docs/Api/AppsApi.md#apptriggerlogs) | **GET** /apps/{app}/trigger-logs | 获取应用触发日志列表
*AppsApi* | [**appUpdate**](docs/Api/AppsApi.md#appupdate) | **PUT** /apps/{app} | 修改应用的基本信息
*AppsApi* | [**appUpdateAccessToken**](docs/Api/AppsApi.md#appupdateaccesstoken) | **PUT** /apps/{app}/tokens | 修改 Access Token
*AppsApi* | [**appUpdateMentor**](docs/Api/AppsApi.md#appupdatementor) | **PATCH** /apps/{app}/mentors | 修改成员备注和权限
*AppsApi* | [**appUpdateMentorOptions**](docs/Api/AppsApi.md#appupdatementoroptions) | **POST** /apps/{app}/mentors-options | 修改应用成员自身的设置（包括应用名备注，是否接收报告等）
*AppsApi* | [**appUpdateMentorReportOptions**](docs/Api/AppsApi.md#appupdatementorreportoptions) | **PATCH** /apps/{app}/mentors-options | 修改应用成员自身的通知设置
*AppsApi* | [**appUpdateOauthApp**](docs/Api/AppsApi.md#appupdateoauthapp) | **PATCH** /apps/{app}/oauth | 修改第三方应用信息
*AppsApi* | [**appUpdateOauthAppLogo**](docs/Api/AppsApi.md#appupdateoauthapplogo) | **PUT** /apps/{app}/oauth | 修改三方应用图标
*AppsApi* | [**appUpdateStatus**](docs/Api/AppsApi.md#appupdatestatus) | **PATCH** /apps/{app} | 修改应用的状态
*FieldsApi* | [**indexFields**](docs/Api/FieldsApi.md#indexfields) | **GET** /indices/{app}/{index}/fields | 获取索引字段映射详情
*FieldsApi* | [**indexUpdateFields**](docs/Api/FieldsApi.md#indexupdatefields) | **POST** /indices/{app}/{index}/fields | 更新索引的字段映射
*FieldsApi* | [**indexUpdateHtmlStripFields**](docs/Api/FieldsApi.md#indexupdatehtmlstripfields) | **PATCH** /indices/{app}/{index}/fields | 更新索引的HTML过滤字段列表
*FieldsApi* | [**indexValuesOfField**](docs/Api/FieldsApi.md#indexvaluesoffield) | **GET** /indices/{app}/{index}/fields/{field} | 获取索引字段的所有值列表
*GlobalApi* | [**json**](docs/Api/GlobalApi.md#json) | **GET** /json | 接口定义(JSON)
*GlobalApi* | [**optionsGet**](docs/Api/GlobalApi.md#optionsget) | **GET** /options | 系统全局配置接口
*GlobalApi* | [**statusDatabase**](docs/Api/GlobalApi.md#statusdatabase) | **GET** /status/database | 数据库服务状态测试
*GlobalApi* | [**statusEngine**](docs/Api/GlobalApi.md#statusengine) | **GET** /status/engine | 搜索引擎状态测试
*GlobalApi* | [**welcome**](docs/Api/GlobalApi.md#welcome) | **GET** / | 接口欢迎信息
*GlobalApi* | [**yaml**](docs/Api/GlobalApi.md#yaml) | **GET** /yaml | 接口定义(YAML)
*IndicesApi* | [**indexCleanup**](docs/Api/IndicesApi.md#indexcleanup) | **POST** /indices/{app}/{index}/cleanup | 清空索引记录
*IndicesApi* | [**indexCopyTo**](docs/Api/IndicesApi.md#indexcopyto) | **POST** /indices/{app}/{index}/copyto | 导出索引数据
*IndicesApi* | [**indexCreate**](docs/Api/IndicesApi.md#indexcreate) | **POST** /indices/{app} | 创建索引
*IndicesApi* | [**indexCreateTemplate**](docs/Api/IndicesApi.md#indexcreatetemplate) | **POST** /indices/templates | 创建索引模板
*IndicesApi* | [**indexDelete**](docs/Api/IndicesApi.md#indexdelete) | **DELETE** /indices/{app}/{index} | 删除索引
*IndicesApi* | [**indexDeleteCrawlerTask**](docs/Api/IndicesApi.md#indexdeletecrawlertask) | **DELETE** /indices/{app}/{index}/crawler-settings | 删除索引的数据爬取任务
*IndicesApi* | [**indexDeleteTemplate**](docs/Api/IndicesApi.md#indexdeletetemplate) | **DELETE** /indices/templates | 删除索引模板
*IndicesApi* | [**indexExport**](docs/Api/IndicesApi.md#indexexport) | **POST** /indices/{app}/{index}/export | 导出索引数据
*IndicesApi* | [**indexFlush**](docs/Api/IndicesApi.md#indexflush) | **POST** /indices/{app}/{index}/flush | 刷新索引数据，主要用于将内存中的索引数据写入磁盘
*IndicesApi* | [**indexFlushSettings**](docs/Api/IndicesApi.md#indexflushsettings) | **PUT** /indices/{app}/{index}/settings | 写入设置信息到索引
*IndicesApi* | [**indexGet**](docs/Api/IndicesApi.md#indexget) | **GET** /indices/{app}/{index} | 获取单个索引详情
*IndicesApi* | [**indexGetCrawlerLogs**](docs/Api/IndicesApi.md#indexgetcrawlerlogs) | **GET** /indices/{app}/{index}/crawler-logs | 获取索引的爬虫任务的爬取日志
*IndicesApi* | [**indexGetCrawlerTask**](docs/Api/IndicesApi.md#indexgetcrawlertask) | **GET** /indices/{app}/{index}/crawler-settings | 获取索引的爬虫任务设定
*IndicesApi* | [**indexGetFilterSettings**](docs/Api/IndicesApi.md#indexgetfiltersettings) | **GET** /indices/{app}/{index}/filter-settings | 获取索引设置信息
*IndicesApi* | [**indexGetSettings**](docs/Api/IndicesApi.md#indexgetsettings) | **GET** /indices/{app}/{index}/settings | 获取索引设置信息
*IndicesApi* | [**indexList**](docs/Api/IndicesApi.md#indexlist) | **GET** /indices/{app} | 获取应用的索引列表
*IndicesApi* | [**indexListTemplates**](docs/Api/IndicesApi.md#indexlisttemplates) | **GET** /indices/templates | 获取所有可用的索引模板
*IndicesApi* | [**indexPrefetch**](docs/Api/IndicesApi.md#indexprefetch) | **GET** /indices/crawler | 获取目标网站内容预览
*IndicesApi* | [**indexRebuild**](docs/Api/IndicesApi.md#indexrebuild) | **POST** /indices/{app}/{index}/rebuild | 重建索引数据
*IndicesApi* | [**indexRebuildTask**](docs/Api/IndicesApi.md#indexrebuildtask) | **GET** /indices/{app}/{index}/rebuild | 获取重建索引任务的详情
*IndicesApi* | [**indexSubmitCrawlerTask**](docs/Api/IndicesApi.md#indexsubmitcrawlertask) | **POST** /indices/{app}/{index}/crawler-settings | 提交或者更新索引的数据爬取任务
*IndicesApi* | [**indexTasks**](docs/Api/IndicesApi.md#indextasks) | **GET** /indices/{app}/tasks | 获取该索引在后台的任务列表
*IndicesApi* | [**indexUpdate**](docs/Api/IndicesApi.md#indexupdate) | **PUT** /indices/{app}/{index} | 修改索引
*IndicesApi* | [**indexUpdateSettings**](docs/Api/IndicesApi.md#indexupdatesettings) | **POST** /indices/{app}/{index}/settings | 更新索引设置信息
*IndicesApi* | [**indexUpdateTemplate**](docs/Api/IndicesApi.md#indexupdatetemplate) | **PUT** /indices/templates | 修改索引模板
*IndicesApi* | [**synonymsAdd**](docs/Api/IndicesApi.md#synonymsadd) | **POST** /indices/{app}/{index}/synonyms | 添加同义词
*IndicesApi* | [**synonymsDelete**](docs/Api/IndicesApi.md#synonymsdelete) | **DELETE** /indices/{app}/{index}/synonyms | 删除同义词
*IndicesApi* | [**synonymsEnable**](docs/Api/IndicesApi.md#synonymsenable) | **PATCH** /indices/{app}/{index}/synonyms | 启用禁用同义词
*IndicesApi* | [**synonymsFlush**](docs/Api/IndicesApi.md#synonymsflush) | **POST** /indices/{app}/{index}/synonyms-flush | 将同义词更新到搜索引擎的同义词表
*IndicesApi* | [**synonymsImport**](docs/Api/IndicesApi.md#synonymsimport) | **POST** /indices/{app}/{index}/synonyms-import | 导入同义词
*IndicesApi* | [**synonymsList**](docs/Api/IndicesApi.md#synonymslist) | **GET** /indices/{app}/{index}/synonyms | 获取索引的所有同义词
*IndicesApi* | [**synonymsUpdate**](docs/Api/IndicesApi.md#synonymsupdate) | **PUT** /indices/{app}/{index}/synonyms | 修改同义词
*MessageApi* | [**messageDelete**](docs/Api/MessageApi.md#messagedelete) | **DELETE** /accounts/message | 删除消息
*MessageApi* | [**messageFeedback**](docs/Api/MessageApi.md#messagefeedback) | **POST** /accounts/feedback | 反馈意见
*MessageApi* | [**messageList**](docs/Api/MessageApi.md#messagelist) | **GET** /accounts/message | 获取我相关的消息信息，包括未读消息数量、最新消息等
*MessageApi* | [**messageRead**](docs/Api/MessageApi.md#messageread) | **PATCH** /accounts/message | 标识消息为已读
*MessageApi* | [**messageSend**](docs/Api/MessageApi.md#messagesend) | **POST** /accounts/message | 发送消息
*PaymentApi* | [**paymentAlipay**](docs/Api/PaymentApi.md#paymentalipay) | **POST** /payment/alipay | 接受支付宝的支付结果
*PaymentApi* | [**paymentAlipayReturn**](docs/Api/PaymentApi.md#paymentalipayreturn) | **GET** /payment/alipay | 支付宝平台支付完毕后调整到该接口
*PaymentApi* | [**paymentApplyInvoice**](docs/Api/PaymentApi.md#paymentapplyinvoice) | **POST** /payment/{app}/invoices | 申请发票
*PaymentApi* | [**paymentBeginPay**](docs/Api/PaymentApi.md#paymentbeginpay) | **POST** /payment/{app}/orders/{ident} | 支付订单
*PaymentApi* | [**paymentBuy**](docs/Api/PaymentApi.md#paymentbuy) | **PUT** /payment/{app}/orders | 下单购买
*PaymentApi* | [**paymentDeleteInvoice**](docs/Api/PaymentApi.md#paymentdeleteinvoice) | **DELETE** /payment/{app}/invoices | 删除发票
*PaymentApi* | [**paymentDeleteOrder**](docs/Api/PaymentApi.md#paymentdeleteorder) | **DELETE** /payment/{app}/orders/{ident} | 取消订单
*PaymentApi* | [**paymentInvoices**](docs/Api/PaymentApi.md#paymentinvoices) | **GET** /payment/{app}/invoices | 获取发票列表
*PaymentApi* | [**paymentOrder**](docs/Api/PaymentApi.md#paymentorder) | **GET** /payment/{app}/orders/{ident} | 获取订单详情
*PaymentApi* | [**paymentOrders**](docs/Api/PaymentApi.md#paymentorders) | **POST** /payment/{app}/orders | 订单列表
*PaymentApi* | [**paymentOrdersWithoutInvoice**](docs/Api/PaymentApi.md#paymentorderswithoutinvoice) | **GET** /payment/{app}/orders_without_invoice | 获取未曾开票的订单列表
*PaymentApi* | [**paymentPrice**](docs/Api/PaymentApi.md#paymentprice) | **GET** /payment/{app}/price | 获取套餐价格
*PaymentApi* | [**paymentReceipt**](docs/Api/PaymentApi.md#paymentreceipt) | **GET** /payment/{app}/orders | 获取订单回执图片
*PaymentApi* | [**paymentRequestContact**](docs/Api/PaymentApi.md#paymentrequestcontact) | **POST** /payment/{app}/contact | 联系销售获取私有化报价
*PaymentApi* | [**paymentUploadReceipt**](docs/Api/PaymentApi.md#paymentuploadreceipt) | **PUT** /payment/{app}/orders/{ident} | 上传转账回执
*PaymentApi* | [**paymentWepay**](docs/Api/PaymentApi.md#paymentwepay) | **POST** /payment/wepay | 接受微信支付的支付结果
*QueriesApi* | [**queryAnalyze**](docs/Api/QueriesApi.md#queryanalyze) | **POST** /queries/{app}/analyze | 分词测试
*QueriesApi* | [**queryCopy**](docs/Api/QueriesApi.md#querycopy) | **POST** /queries/{app}/copy | 复制指定查询并创建新查询
*QueriesApi* | [**queryCopyToQuery**](docs/Api/QueriesApi.md#querycopytoquery) | **PUT** /queries/{app}/copy | 复制查询到已有查询
*QueriesApi* | [**queryCreate**](docs/Api/QueriesApi.md#querycreate) | **POST** /queries/{app} | 创建搜索
*QueriesApi* | [**queryCreateKeywordBindings**](docs/Api/QueriesApi.md#querycreatekeywordbindings) | **POST** /queries/{app}/keyword-bindings | 创建新的关键词文档绑定
*QueriesApi* | [**queryCreateVariable**](docs/Api/QueriesApi.md#querycreatevariable) | **POST** /queries/{app}/variables | 创建新的预定义查询变量
*QueriesApi* | [**queryDelete**](docs/Api/QueriesApi.md#querydelete) | **DELETE** /queries/{app} | 删除搜索
*QueriesApi* | [**queryDeleteKeywordBindings**](docs/Api/QueriesApi.md#querydeletekeywordbindings) | **DELETE** /queries/{app}/keyword-bindings | 删除关键词文档绑定
*QueriesApi* | [**queryDeleteNodePositions**](docs/Api/QueriesApi.md#querydeletenodepositions) | **DELETE** /queries/{app}/node-positions | 清除查询条件的节点位置信息
*QueriesApi* | [**queryDeleteVariable**](docs/Api/QueriesApi.md#querydeletevariable) | **DELETE** /queries/{app}/variables | 删除预定义查询变量
*QueriesApi* | [**queryFields**](docs/Api/QueriesApi.md#queryfields) | **GET** /queries/{app}/fields | 获取查询关联的所有索引的字段信息
*QueriesApi* | [**queryGet**](docs/Api/QueriesApi.md#queryget) | **GET** /queries/{app}/{query} | 获取查询的详情
*QueriesApi* | [**queryGetNodePositions**](docs/Api/QueriesApi.md#querygetnodepositions) | **GET** /queries/{app}/node-positions | 获取查询条件的节点位置信息
*QueriesApi* | [**queryGetRecord**](docs/Api/QueriesApi.md#querygetrecord) | **GET** /queries/{app}/record | 获取记录的详情
*QueriesApi* | [**queryKeywordBindings**](docs/Api/QueriesApi.md#querykeywordbindings) | **GET** /queries/{app}/keyword-bindings | 获取查询的关键词文档绑定列表
*QueriesApi* | [**queryList**](docs/Api/QueriesApi.md#querylist) | **GET** /queries/{app} | 获取应用下所有索引下的查询列表（按索引进行分组）
*QueriesApi* | [**queryProfile**](docs/Api/QueriesApi.md#queryprofile) | **GET** /queries/{app}/profiler | 获取搜索诊断信息
*QueriesApi* | [**queryRecordsOfKeywordBinding**](docs/Api/QueriesApi.md#queryrecordsofkeywordbinding) | **GET** /queries/{app}/keyword-bindings-records | 获取关键词绑定对应的记录列表
*QueriesApi* | [**querySaveIntelligentMappings**](docs/Api/QueriesApi.md#querysaveintelligentmappings) | **PUT** /queries/{app}/intelligent-mappings | 设置索引智能匹配字段
*QueriesApi* | [**querySaveNodePositions**](docs/Api/QueriesApi.md#querysavenodepositions) | **PUT** /queries/{app}/node-positions | 保存查询条件的节点位置信息
*QueriesApi* | [**querySearch**](docs/Api/QueriesApi.md#querysearch) | **GET** /queries/{app}/search | 搜索测试
*QueriesApi* | [**querySource**](docs/Api/QueriesApi.md#querysource) | **POST** /queries/{app}/{query} | 获取最终查询的源码(JSON)
*QueriesApi* | [**querySuggest**](docs/Api/QueriesApi.md#querysuggest) | **GET** /queries/{app}/suggest | 获取搜索建议列表
*QueriesApi* | [**queryTestIntelligentMappings**](docs/Api/QueriesApi.md#querytestintelligentmappings) | **POST** /queries/{app}/intelligent-mappings | 测试索引智能匹配字段
*QueriesApi* | [**queryUpdate**](docs/Api/QueriesApi.md#queryupdate) | **PUT** /queries/{app} | 修改查询
*QueriesApi* | [**queryUpdateKeywordBindings**](docs/Api/QueriesApi.md#queryupdatekeywordbindings) | **PATCH** /queries/{app}/keyword-bindings | 修改关键词文档绑定
*QueriesApi* | [**queryUpdateSettings**](docs/Api/QueriesApi.md#queryupdatesettings) | **POST** /queries/{app}/settings | 更改查询的设置项
*QueriesApi* | [**queryUpdateVariable**](docs/Api/QueriesApi.md#queryupdatevariable) | **PATCH** /queries/{app}/variables | 修改预定义查询变量
*QueriesApi* | [**queryValidate**](docs/Api/QueriesApi.md#queryvalidate) | **GET** /queries/{app}/validate | 获取搜索验证结果
*QueriesApi* | [**queryValidateAggregation**](docs/Api/QueriesApi.md#queryvalidateaggregation) | **POST** /queries/{app}/validate-aggregation | 验证聚合定义是否正确
*QueriesApi* | [**queryValidateQuery**](docs/Api/QueriesApi.md#queryvalidatequery) | **POST** /queries/{app}/validate-query | 验证聚合定义是否正确
*QueriesApi* | [**queryValidateScriptField**](docs/Api/QueriesApi.md#queryvalidatescriptfield) | **POST** /queries/{app}/validate-script-field | 验证脚本字段是否正确
*QueriesApi* | [**queryValidateScriptScore**](docs/Api/QueriesApi.md#queryvalidatescriptscore) | **POST** /queries/{app}/validate-script-score | 验证脚本字段是否正确
*QueriesApi* | [**queryValidateSuggestion**](docs/Api/QueriesApi.md#queryvalidatesuggestion) | **POST** /queries/{app}/validate-suggest | 验证建议是否正确
*QueriesApi* | [**queryVariables**](docs/Api/QueriesApi.md#queryvariables) | **GET** /queries/{app}/variables | 获取应用的预定义查询变量列表
*RecommendApi* | [**recommendClick**](docs/Api/RecommendApi.md#recommendclick) | **POST** /recommend/{ident}/click | 推荐结果点击行为收集
*RecommendApi* | [**recommendCreate**](docs/Api/RecommendApi.md#recommendcreate) | **POST** /recommends/{app} | 创建新的推荐
*RecommendApi* | [**recommendDelete**](docs/Api/RecommendApi.md#recommenddelete) | **DELETE** /recommends/{app} | 删除推荐
*RecommendApi* | [**recommendDetail**](docs/Api/RecommendApi.md#recommenddetail) | **GET** /recommend/{ident} | 获取推荐的记录列表
*RecommendApi* | [**recommendFetch**](docs/Api/RecommendApi.md#recommendfetch) | **POST** /recommend/{ident} | 获取推荐的记录列表
*RecommendApi* | [**recommendList**](docs/Api/RecommendApi.md#recommendlist) | **GET** /recommends/{app} | 获取已定义的推荐列表
*RecommendApi* | [**recommendUpdate**](docs/Api/RecommendApi.md#recommendupdate) | **PUT** /recommends/{app} | 更新推荐信息
*RecordsApi* | [**recordDelete**](docs/Api/RecordsApi.md#recorddelete) | **DELETE** /records/{app}/{index} | 删除记录数据
*RecordsApi* | [**recordDeleteByQuery**](docs/Api/RecordsApi.md#recorddeletebyquery) | **DELETE** /records/{app}/{index}/bulk | 批量删除记录数据(delete_by_query)，该接口传递查询参数获取要删除的记录并逐一删除
*RecordsApi* | [**recordGet**](docs/Api/RecordsApi.md#recordget) | **GET** /records/{app}/{index} | 获取单条记录详情
*RecordsApi* | [**recordList**](docs/Api/RecordsApi.md#recordlist) | **POST** /records/{app}/{index} | 获取索引记录列表
*RecordsApi* | [**recordPush**](docs/Api/RecordsApi.md#recordpush) | **PUT** /records/{app}/{index} | 插入或者更新索引数据
*RecordsApi* | [**recordUpdateByQuery**](docs/Api/RecordsApi.md#recordupdatebyquery) | **PATCH** /records/{app}/{index}/bulk | 批量修改记录数据(update_by_query)，该接口传递查询参数获取要更新的记录，并使用 body 中的对象进行记录合并更新
*RecordsApi* | [**recordUpload**](docs/Api/RecordsApi.md#recordupload) | **POST** /records/{app}/{index}/bulk | 上传记录
*RecordsApi* | [**recordUploadOld**](docs/Api/RecordsApi.md#recorduploadold) | **POST** /records/{app}/{index}/upload | 上传记录，该接口已经废弃，请使用 /records/{app}/{index}/bulk 接口
*SearchApi* | [**searchClick**](docs/Api/SearchApi.md#searchclick) | **POST** /search/widget/{widget}/click | 搜索结果点击行为收集
*SearchApi* | [**searchHistories**](docs/Api/SearchApi.md#searchhistories) | **GET** /search/widget/{widget}/histories | 获取当前搜索用户的最新搜索记录
*SearchApi* | [**searchLogs**](docs/Api/SearchApi.md#searchlogs) | **GET** /apps/{app}/logs-searchs | 获取搜索日志
*SearchApi* | [**searchQueryClick**](docs/Api/SearchApi.md#searchqueryclick) | **POST** /search/query/{query}/click | 搜索结果点击行为收集
*SearchApi* | [**searchQueryHistories**](docs/Api/SearchApi.md#searchqueryhistories) | **GET** /search/query/{query}/histories | 获取当前搜索用户的最新搜索记录
*SearchApi* | [**searchQueryHotWords**](docs/Api/SearchApi.md#searchqueryhotwords) | **GET** /search/query/{query}/hotwords | 获取查询相关热词
*SearchApi* | [**searchQueryRepeatScroll**](docs/Api/SearchApi.md#searchqueryrepeatscroll) | **POST** /search/scroll/{query} | 读取滚动搜索结果
*SearchApi* | [**searchQueryScroll**](docs/Api/SearchApi.md#searchqueryscroll) | **GET** /search/scroll/{query} | 基于查询的滚动搜索
*SearchApi* | [**searchQuerySearch**](docs/Api/SearchApi.md#searchquerysearch) | **GET** /search/query/{query} | 基于查询的公开搜索
*SearchApi* | [**searchWidgetAutoComplete**](docs/Api/SearchApi.md#searchwidgetautocomplete) | **GET** /search/widget/{widget}/autocomplete | 基于组件的搜索词自动完成
*SearchApi* | [**searchWidgetHotWords**](docs/Api/SearchApi.md#searchwidgethotwords) | **GET** /search/widget/{widget}/hotwords | 获取组件搜索的相关热词
*SearchApi* | [**searchWidgetSearch**](docs/Api/SearchApi.md#searchwidgetsearch) | **GET** /search/widget/{widget} | 基于组件的公开搜索
*StatsApi* | [**statsRecomms**](docs/Api/StatsApi.md#statsrecomms) | **GET** /stats/{app}/recomms | 获取推荐日志的汇总信息
*StatsApi* | [**statsSearchs**](docs/Api/StatsApi.md#statssearchs) | **GET** /stats/{app}/searchs | 获取搜索日志的汇总信息
*StatsApi* | [**statsTopClicks**](docs/Api/StatsApi.md#statstopclicks) | **GET** /stats/{app}/top-clicks | 获取点击排行榜
*StatsApi* | [**statsWidgets**](docs/Api/StatsApi.md#statswidgets) | **GET** /stats/{app}/widgets | 获取模板与组件的统计信息
*WidgetsApi* | [**widgetCopy**](docs/Api/WidgetsApi.md#widgetcopy) | **POST** /widget/{app}/copy | 复制指定组件并创建新组件
*WidgetsApi* | [**widgetCopyToWidget**](docs/Api/WidgetsApi.md#widgetcopytowidget) | **PUT** /widget/{app}/copy | 复制组件到已有组件
*WidgetsApi* | [**widgetCreate**](docs/Api/WidgetsApi.md#widgetcreate) | **POST** /widgets/{app} | 创建组件
*WidgetsApi* | [**widgetDelete**](docs/Api/WidgetsApi.md#widgetdelete) | **DELETE** /widgets/{app}/{widget} | 删除组件
*WidgetsApi* | [**widgetDeleteLogo**](docs/Api/WidgetsApi.md#widgetdeletelogo) | **DELETE** /widgets/{app}/{widget}/logo | 删除组件 Logo
*WidgetsApi* | [**widgetDetail**](docs/Api/WidgetsApi.md#widgetdetail) | **GET** /widget/{ident} | 获取UI组件的所有相关信息
*WidgetsApi* | [**widgetDownload**](docs/Api/WidgetsApi.md#widgetdownload) | **GET** /widgets/{app}/{widget}/download | 下载组件应用源码
*WidgetsApi* | [**widgetGet**](docs/Api/WidgetsApi.md#widgetget) | **GET** /widgets/{app}/{widget} | 获取组件的详情
*WidgetsApi* | [**widgetList**](docs/Api/WidgetsApi.md#widgetlist) | **GET** /widgets/{app} | 获取应用的组件列表
*WidgetsApi* | [**widgetLogo**](docs/Api/WidgetsApi.md#widgetlogo) | **POST** /widgets/{app}/{widget}/logo | 设置组件 Logo
*WidgetsApi* | [**widgetUpdate**](docs/Api/WidgetsApi.md#widgetupdate) | **PUT** /widgets/{app}/{widget} | 修改组件
*WidgetsApi* | [**widgetUpdateSettings**](docs/Api/WidgetsApi.md#widgetupdatesettings) | **PATCH** /widgets/{app}/{widget} | 修改组件设置参数

## Models

- [AccountBean](docs/Model/AccountBean.md)
- [AnalyzeObject](docs/Model/AnalyzeObject.md)
- [AnalyzeToken](docs/Model/AnalyzeToken.md)
- [AppBean](docs/Model/AppBean.md)
- [AppLogAccount](docs/Model/AppLogAccount.md)
- [AppLogBean](docs/Model/AppLogBean.md)
- [AppLogsBean](docs/Model/AppLogsBean.md)
- [AppMentorBean](docs/Model/AppMentorBean.md)
- [AutoCompleteItem](docs/Model/AutoCompleteItem.md)
- [BlacklistBean](docs/Model/BlacklistBean.md)
- [Bulletin](docs/Model/Bulletin.md)
- [CompanyBean](docs/Model/CompanyBean.md)
- [ContactForm](docs/Model/ContactForm.md)
- [CrawlerLog](docs/Model/CrawlerLog.md)
- [CrawlerLogs](docs/Model/CrawlerLogs.md)
- [CrawlerTask](docs/Model/CrawlerTask.md)
- [GlobalOptionForm](docs/Model/GlobalOptionForm.md)
- [IndexBean](docs/Model/IndexBean.md)
- [IndexFieldBean](docs/Model/IndexFieldBean.md)
- [IndexForm](docs/Model/IndexForm.md)
- [IndexRebuildForm](docs/Model/IndexRebuildForm.md)
- [IndexSettings](docs/Model/IndexSettings.md)
- [IndexStatBean](docs/Model/IndexStatBean.md)
- [IndexTask](docs/Model/IndexTask.md)
- [IndexTemplate](docs/Model/IndexTemplate.md)
- [IndexTemplates](docs/Model/IndexTemplates.md)
- [IntelligentMapping](docs/Model/IntelligentMapping.md)
- [KeywordBindingBean](docs/Model/KeywordBindingBean.md)
- [MentorForm](docs/Model/MentorForm.md)
- [Message](docs/Model/Message.md)
- [Messages](docs/Model/Messages.md)
- [OauthAppBean](docs/Model/OauthAppBean.md)
- [OpenidBean](docs/Model/OpenidBean.md)
- [PayResult](docs/Model/PayResult.md)
- [PaymentInvoice](docs/Model/PaymentInvoice.md)
- [PaymentOrder](docs/Model/PaymentOrder.md)
- [PaymentRecord](docs/Model/PaymentRecord.md)
- [PaymentService](docs/Model/PaymentService.md)
- [QueryActionBean](docs/Model/QueryActionBean.md)
- [QueryBean](docs/Model/QueryBean.md)
- [QueryForm](docs/Model/QueryForm.md)
- [QueryNode](docs/Model/QueryNode.md)
- [QuerySortField](docs/Model/QuerySortField.md)
- [QueryVariableBean](docs/Model/QueryVariableBean.md)
- [RecommendBean](docs/Model/RecommendBean.md)
- [RecordFilter](docs/Model/RecordFilter.md)
- [SearchEstimateResult](docs/Model/SearchEstimateResult.md)
- [SearchWord](docs/Model/SearchWord.md)
- [StatIndexBean](docs/Model/StatIndexBean.md)
- [SynonymsBean](docs/Model/SynonymsBean.md)
- [TokenBean](docs/Model/TokenBean.md)
- [TriggerBean](docs/Model/TriggerBean.md)
- [TriggerLogBean](docs/Model/TriggerLogBean.md)
- [ValueOfField](docs/Model/ValueOfField.md)
- [WidgetBean](docs/Model/WidgetBean.md)
- [WidgetForm](docs/Model/WidgetForm.md)
- [WidgetLogo](docs/Model/WidgetLogo.md)
- [WidgetStatusForm](docs/Model/WidgetStatusForm.md)

## Authorization

Authentication schemes defined for the API:
### TokenAuth

- **Type**: Bearer authentication (Random)

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author

indexea.com@gmail.com

## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `1.0.0`
    - Package version: `1.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
